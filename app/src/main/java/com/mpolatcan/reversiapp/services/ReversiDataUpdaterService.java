package com.mpolatcan.reversiapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mpolatcan.reversiapp.R;
import com.mpolatcan.reversiapp.ReversiBaseApplication;
import com.mpolatcan.reversiapp.models.SoundOptionsModel;
import com.mpolatcan.reversiapp.models.StatisticsDataModel;
import com.mpolatcan.reversiapp.models.UserOptionsModel;
import com.mpolatcan.reversiapp.utils.ReversiConstants;
import com.mpolatcan.reversiapp.utils.StatisticsDBHelper;
import com.securepreferences.SecurePreferences;

/**
 * Created by mpolatcan-gyte_cse on 01.08.2016.
 */
public class ReversiDataUpdaterService extends IntentService {
    private SecurePreferences statisticsData;
    private SharedPreferences userOptions,
                              soundOptions;
    private StatisticsDBHelper statisticsDBHelper;
    private ReversiBaseApplication reversiApp;
    private UserOptionsModel userOptionsModel;
    private StatisticsDataModel statisticsDataModel;
    private SoundOptionsModel soundOptionsModel;

    public ReversiDataUpdaterService() {
        super(ReversiDataUpdaterService.class.getName());
    }

    @Override
    public void onCreate() {
        super.onCreate();

        reversiApp = ReversiBaseApplication.getInstance();

        userOptionsModel = reversiApp.getUserOptions();
        statisticsDataModel = reversiApp.getStatisticsData();
        soundOptionsModel = reversiApp.getSoundOptions();

        statisticsData = new SecurePreferences(
                ReversiBaseApplication.getInstance().getApplicationContext(),
                ReversiConstants.SPREF_PASSWORD,
                ReversiConstants.STATISTICS_DATA);
        userOptions = getSharedPreferences(ReversiConstants.USER_OPTIONS, MODE_PRIVATE);
        soundOptions = getSharedPreferences(ReversiConstants.SOUND_OPTIONS, MODE_PRIVATE);
        statisticsDBHelper = new StatisticsDBHelper(ReversiDataUpdaterService.this);
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        if (intent.getAction().equals(ReversiConstants.ACTION_FETCH_DATA))
            fetchDatas();
        else if (intent.getAction().equals(ReversiConstants.ACTION_UPDATE_STAT_DATA))
            saveStatisticsData(intent.getIntExtra(ReversiConstants.RESULT, -1),
                    intent.getIntExtra(ReversiConstants.USER_CELL_NUM, -1),
                    intent.getStringExtra(ReversiConstants.SELECTED_USER_AVATAR_NAME),
                    intent.getStringExtra(ReversiConstants.SELECTED_CPU_AVATAR_NAME));
        else if (intent.getAction().equals(ReversiConstants.ACTION_COMMIT_DATA))
            commitDatas();

        stopSelf();
    }

    public void fetchDatas() {
        SharedPreferences.Editor statisticsDataEditor = statisticsData.edit();
        SharedPreferences.Editor soundOptionsEditor = soundOptions.edit();
        SharedPreferences.Editor userOptionsEditor = userOptions.edit();

        int winNumber = statisticsData.getInt(ReversiConstants.WIN_NUMBER, -1);
        int lostNumber = statisticsData.getInt(ReversiConstants.LOST_NUMBER, -1);
        int highScore = statisticsData.getInt(ReversiConstants.HIGH_SCORE, -1);
        int mostPlayedAvatar = statisticsData.getInt(ReversiConstants.MOST_PLAYED_AVATAR, -1);
        int mostBeatenAvatar = statisticsData.getInt(ReversiConstants.MOST_BEATEN_AVATAR, -1);
        int sfxVolume = soundOptions.getInt(ReversiConstants.SFX_VOLUME, -1);
        int musicVolume = soundOptions.getInt(ReversiConstants.MUSIC_VOLUME, -1);
        int lastSelectedAvatar = userOptions.getInt(ReversiConstants.LAST_SELECTED_AVATAR, -1);
        int lastSelectedBoard = userOptions.getInt(ReversiConstants.LAST_SELECTED_BOARD, -1);
        int isAvatarUnlocked = userOptions.getInt(ReversiConstants.IS_AVATAR_UNLOCKED, -1);
        int unlockedAvatarIndex = userOptions.getInt(ReversiConstants.UNLOCKED_AVATAR_INDEX, -1);

        // -------- Initialize statistics datas ------------------------------
        if (winNumber == -1) {
            statisticsDataModel.setWinNumber(0);
            statisticsDataEditor.putInt(ReversiConstants.WIN_NUMBER, 0);
        } else
            statisticsDataModel.setWinNumber(winNumber);

        if (lostNumber == -1) {
            statisticsDataModel.setLostNumber(0);
            statisticsDataEditor.putInt(ReversiConstants.LOST_NUMBER, 0);
        } else
            statisticsDataModel.setLostNumber(lostNumber);

        if (highScore == -1) {
            statisticsDataModel.setHighScore(0);
            statisticsDataEditor.putInt(ReversiConstants.HIGH_SCORE, 0);
        } else
            statisticsDataModel.setHighScore(highScore);

        if (mostPlayedAvatar == -1) {
            statisticsDataModel.setMostPlayedAvatar(-1);
            statisticsDataEditor.putInt(ReversiConstants.MOST_PLAYED_AVATAR, -1);
        } else
            statisticsDataModel.setMostPlayedAvatar(mostPlayedAvatar);

        if (mostBeatenAvatar == -1) {
            statisticsDataModel.setMostBeatenAvatar(-1);
            statisticsDataEditor.putInt(ReversiConstants.MOST_BEATEN_AVATAR, -1);
        } else
            statisticsDataModel.setMostBeatenAvatar(mostBeatenAvatar);

        statisticsDataEditor.apply();
        // -------------------------------------------------------------------

        // ------- Initialize sound options datas ----------------------------
        if (sfxVolume == -1) {
            soundOptionsModel.setSfxVolume(100);
            soundOptionsEditor.putInt(ReversiConstants.SFX_VOLUME, 100);
        } else
            soundOptionsModel.setSfxVolume(sfxVolume);

        if (musicVolume == -1) {
            soundOptionsModel.setMusicVolume(100);
            soundOptionsEditor.putInt(ReversiConstants.MUSIC_VOLUME, 100);
        } else
            soundOptionsModel.setMusicVolume(musicVolume);

        soundOptionsEditor.apply();
        // ------------------------------------------------------------------

        // ------ Initialize user options datas -----------------------------
        if (lastSelectedAvatar == -1) {
            userOptionsModel.setLastSelectedAvatar(0);
            userOptionsEditor.putInt(ReversiConstants.LAST_SELECTED_AVATAR, 0);
        } else
            userOptionsModel.setLastSelectedAvatar(lastSelectedAvatar);

        if (lastSelectedBoard == -1) {
            userOptionsModel.setLastSelectedBoard(0);
            userOptionsEditor.putInt(ReversiConstants.LAST_SELECTED_BOARD, 0);
        } else
            userOptionsModel.setLastSelectedBoard(lastSelectedBoard);

        if (isAvatarUnlocked == -1) {
            userOptionsModel.setIsAvatarUnlocked(ReversiConstants.NOT_UNLOCKED);
            userOptionsEditor.putInt(ReversiConstants.IS_AVATAR_UNLOCKED, ReversiConstants.NOT_UNLOCKED);
        } else
            userOptionsModel.setIsAvatarUnlocked(isAvatarUnlocked);

        if (unlockedAvatarIndex == -1) {
            userOptionsModel.setUnlockedAvatarIndex(0);
            userOptionsEditor.putInt(ReversiConstants.UNLOCKED_AVATAR_INDEX, 0);
        }

        userOptionsEditor.apply();
        // ------------------------------------------------------------------

        Log.d("DATA-UPDATER", "Data fetched... WIN: " + statisticsDataModel.getWinNumber());
        Log.d("DATA-UPDATER", "Data fetched... LOST: " + statisticsDataModel.getLostNumber());
        Log.d("DATA-UPDATER", "Data fetched... HIGH SCORE: " + statisticsDataModel.getHighScore());
        Log.d("DATA-UPDATER", "Data fetched... SFX VOLUME: " + soundOptionsModel.getSfxVolume());
        Log.d("DATA-UPDATER", "Data fetched... MUSIC VOLUME: " + soundOptionsModel.getMusicVolume());
        Log.d("DATA-UPDATER", "Data fetched... LAST SELECTED AVATAR: " + userOptionsModel.getLastSelectedAvatar());
        Log.d("DATA-UPDATER", "Data fetched... LAST SELECTED BOARD: " + userOptionsModel.getLastSelectedBoard());
        Log.d("DATA-UPDATER", "Data fetched... IS_AVATAR_UNLOCKED:" + userOptionsModel.getIsAvatarUnlocked());
        Log.d("DATA-UPDATER", "Data fetched... UNLOCKED AVATAR INDEX: " + userOptionsModel.getUnlockedAvatarIndex());
    }

    public void commitDatas() {
        SharedPreferences.Editor soundOptionsEditor = soundOptions.edit();
        SharedPreferences.Editor userOptionsEditor = userOptions.edit();

        soundOptionsEditor.putInt(ReversiConstants.SFX_VOLUME, soundOptionsModel.getSfxVolume());
        soundOptionsEditor.putInt(ReversiConstants.MUSIC_VOLUME, soundOptionsModel.getMusicVolume());
        soundOptionsEditor.apply();

        userOptionsEditor.putInt(ReversiConstants.LAST_SELECTED_AVATAR, userOptionsModel.getLastSelectedAvatar());
        userOptionsEditor.putInt(ReversiConstants.LAST_SELECTED_BOARD, userOptionsModel.getLastSelectedAvatar());
        userOptionsEditor.putInt(ReversiConstants.IS_AVATAR_UNLOCKED, userOptionsModel.getIsAvatarUnlocked());
        userOptionsEditor.putInt(ReversiConstants.UNLOCKED_AVATAR_INDEX, userOptionsModel.getUnlockedAvatarIndex());

        userOptionsEditor.apply();

        Log.d("DATA-UPDATER", "Data committed... WIN: " + statisticsDataModel.getWinNumber());
        Log.d("DATA-UPDATER", "Data committed... LOST: " + statisticsDataModel.getLostNumber());
        Log.d("DATA-UPDATER", "Data committed... HIGH SCORE: " + statisticsDataModel.getHighScore());
        Log.d("DATA-UPDATER", "Data committed... SFX VOLUME: " + soundOptionsModel.getSfxVolume());
        Log.d("DATA-UPDATER", "Data committed... MUSIC VOLUME: " + soundOptionsModel.getMusicVolume());
        Log.d("DATA-UPDATER", "Data committed... LAST SELECTED AVATAR: " + userOptionsModel.getLastSelectedAvatar());
        Log.d("DATA-UPDATER", "Data committed... LAST SELECTED BOARD: " + userOptionsModel.getLastSelectedBoard());
        Log.d("DATA-UPDATER", "Data committed... IS_AVATAR_UNLOCKED:" + userOptionsModel.getIsAvatarUnlocked());
        Log.d("DATA-UPDATER", "Data committed... UNLOCKED AVATAR INDEX: " + userOptionsModel.getUnlockedAvatarIndex());
    }

    public void saveStatisticsData(final Integer result,
                                   final Integer userCellNum,
                                   final String selectedUserAvatarName,
                                   final String selectedComputerAvatarName) {
        SharedPreferences.Editor statisticsEditor = statisticsData.edit();
        SQLiteDatabase statisticsDatabase = statisticsDBHelper.getWritableDatabase();

        if (result == ReversiConstants.WIN) {
            if (statisticsData.getInt(ReversiConstants.HIGH_SCORE, -1) < userCellNum) {
                statisticsDataModel.setHighScore(userCellNum);
                statisticsEditor.putInt(ReversiConstants.HIGH_SCORE, userCellNum);
                Log.d("DATA-UPDATER", "Data updated... HIGH SCORE: " + userCellNum);
            }

            int winNumber = statisticsData.getInt(ReversiConstants.WIN_NUMBER, -1);
            Log.d("WIN NUM", "" + winNumber);

            if (winNumber > 0) {
                int[] winValues = getResources().getIntArray(R.array.reversi_avatar_win_values);

                for (int i = 0; i < winValues.length; ++i) {
                    if ((winNumber + 1) == winValues[i]) {
                        userOptionsModel.setLastSelectedAvatar(
                                userOptionsModel.getLastSelectedAvatar());
                        userOptionsModel.setLastSelectedBoard(
                                userOptionsModel.getLastSelectedBoard());
                        userOptionsModel.setIsAvatarUnlocked(ReversiConstants.UNLOCKED);
                        userOptionsModel.setUnlockedAvatarIndex(i);
                    }
                }
            }

            statisticsDataModel.setWinNumber(winNumber + 1);
            statisticsEditor.putInt(ReversiConstants.WIN_NUMBER,
                    statisticsData.getInt(ReversiConstants.WIN_NUMBER, -1) + 1);
            Log.d("DATA-UPDATER", "Data updated... WIN NUMBER: " + statisticsDataModel.getWinNumber());

            // Increment beat rate of computer's avatar
            statisticsDatabase
                    .execSQL("UPDATE " + ReversiConstants.TABLE_NAME + " SET " + ReversiConstants.BEAT_RATE +
                            " = " + ReversiConstants.BEAT_RATE + " + 1 WHERE " + ReversiConstants.AVATAR_NAME +
                            "='" + selectedComputerAvatarName + "'");
        } else if (result == ReversiConstants.LOST) {
            int lostNumber = statisticsData.getInt(ReversiConstants.LOST_NUMBER, -1);
            statisticsDataModel.setLostNumber(lostNumber + 1);
            statisticsEditor.putInt(ReversiConstants.LOST_NUMBER, lostNumber + 1);
            Log.d("DATA-UPDATER", "Data updated... LOST NUMBER: " + statisticsDataModel.getLostNumber());

        }

        // Increment play rate of user's avatar
        statisticsDatabase
                .execSQL("UPDATE " + ReversiConstants.TABLE_NAME + " SET " + ReversiConstants.PLAY_RATE +
                        " = " + ReversiConstants.PLAY_RATE + " + 1 WHERE " + ReversiConstants.AVATAR_NAME +
                        "='" + selectedUserAvatarName + "'");
        statisticsDatabase.close();

        statisticsDatabase = statisticsDBHelper.getReadableDatabase();

        Cursor getMaxPlayRate = statisticsDatabase.
                rawQuery("SELECT * FROM " + ReversiConstants.TABLE_NAME + " ORDER BY " +
                        ReversiConstants.PLAY_RATE + " DESC LIMIT 1", null);

        Cursor getMaxBeatRate = statisticsDatabase.
                rawQuery("SELECT * FROM " + ReversiConstants.TABLE_NAME + " ORDER BY " +
                        ReversiConstants.BEAT_RATE + " DESC LIMIT 1", null);

        getMaxPlayRate.moveToFirst();
        getMaxBeatRate.moveToFirst();

        int mostPlayedAvatarId = getResources()
                .getIdentifier(getMaxPlayRate.getString(getMaxPlayRate.getColumnIndex(ReversiConstants.AVATAR_NAME)),
                        "drawable",
                        getPackageName());

        int mostBeatenAvatarId = getResources()
                .getIdentifier(getMaxBeatRate.getString(getMaxBeatRate.getColumnIndex(ReversiConstants.AVATAR_NAME)),
                        "drawable",
                        getPackageName());


        statisticsDataModel.setMostPlayedAvatar(mostPlayedAvatarId);
        statisticsEditor.putInt(ReversiConstants.MOST_PLAYED_AVATAR, mostPlayedAvatarId);
        Log.d("DATA-UPDATER", "Data updated... MOST PLAYED AVATAR: " + mostPlayedAvatarId);

        statisticsDataModel.setMostBeatenAvatar(mostBeatenAvatarId);
        statisticsEditor.putInt(ReversiConstants.MOST_BEATEN_AVATAR, mostBeatenAvatarId);
        Log.d("DATA-UPDATER", "Data updated... MOST BEATEN AVATAR: " + mostBeatenAvatarId);

        statisticsEditor.apply();
        getMaxPlayRate.close();
        getMaxBeatRate.close();

        statisticsDatabase.close();
    }
}
