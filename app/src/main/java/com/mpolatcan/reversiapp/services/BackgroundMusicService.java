package com.mpolatcan.reversiapp.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.mpolatcan.reversiapp.R;
import com.mpolatcan.reversiapp.ReversiBaseApplication;
import com.mpolatcan.reversiapp.utils.ReversiConstants;

public class BackgroundMusicService extends Service {
    private MediaPlayer mediaPlayer;
    private ReversiBaseApplication reversiApp;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        reversiApp = ReversiBaseApplication.getInstance();

        mediaPlayer = MediaPlayer.create(this, R.raw.bg_music);
        mediaPlayer.setLooping(true);

        setBgMusicVolume(reversiApp.getSoundOptions().getMusicVolume());

        mediaPlayer.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (intent.getAction().equals(ReversiConstants.ACTION_PLAY_MUSIC))
                startMusic();
            else if (intent.getAction().equals(ReversiConstants.ACTION_PAUSE_MUSIC))
                pauseMusic();
            else if (intent.getAction().equals(ReversiConstants.ACTION_SET_MUSIC_VOLUME))
                setBgMusicVolume(intent.getIntExtra(ReversiConstants.NEW_MUSIC_VOLUME, 100));
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("BG-MUSIC", "Music terminated");
        mediaPlayer.stop();
        mediaPlayer.release();
        super.onDestroy();
    }

    public void setBgMusicVolume(int volume) {
        final float newVolume = (float) (1 - (Math.log(ReversiConstants.MAX_VOLUME - volume)
                                        / Math.log(ReversiConstants.MAX_VOLUME)));
        mediaPlayer.setVolume(newVolume, newVolume);
        Log.d("BG-MUSIC", "Music volume changed to: " + volume);
    }

    public void pauseMusic() {
        Log.d("BG-MUSIC", "Music paused");
        mediaPlayer.pause();
    }

    public void startMusic() {
        Log.d("BG-MUSIC", "Music started");
        mediaPlayer.start();
    }
}
