package com.mpolatcan.reversiapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.mpolatcan.reversiapp.R;
import com.mpolatcan.reversiapp.ReversiBaseApplication;
import com.mpolatcan.reversiapp.models.SoundOptionsModel;
import com.mpolatcan.reversiapp.services.BackgroundMusicService;
import com.mpolatcan.reversiapp.utils.ReversiConstants;
import com.mpolatcan.reversiapp.customviews.CircularSeekBar;

import io.fabric.sdk.android.Fabric;

public class OptionsActivity extends Activity {
    private CircularSeekBar soundFxVolume, musicVolume;
    private TextView soundFxVolumeVal,
                     musicVolumeVal;
    private ImageButton backToMainMenuBtn;
    private ReversiBaseApplication reversiApp;
    private SoundOptionsModel soundOptions;
    private Intent bgMusicServiceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        // This code will be deleted in release. That's for performance analysis
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        // ---------------------------------------------------------------------

        Fabric.with(this, new Crashlytics());

        soundFxVolume = (CircularSeekBar) findViewById(R.id.sound_fx_volume);
        musicVolume = (CircularSeekBar) findViewById(R.id.music_volume);
        musicVolumeVal = (TextView) findViewById(R.id.music_volume_val);
        soundFxVolumeVal = (TextView) findViewById(R.id.sound_fx_volume_val);
        backToMainMenuBtn = (ImageButton) findViewById(R.id.options_back_btn);

        // --------------- Setting for WVGA800 screens --------------------------------------
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        View bigCircleFirst = findViewById(R.id.big_circle_first);
        View bigCircleSecond = findViewById(R.id.big_circle_second);
        View smallCircleFirst = findViewById(R.id.small_circle_first);
        View smallCircleSecond = findViewById(R.id.small_circle_second);

        reversiApp = ReversiBaseApplication.getInstance();
        soundOptions = reversiApp.getSoundOptions();

        bgMusicServiceIntent = new Intent(this, BackgroundMusicService.class);
        bgMusicServiceIntent.setAction(ReversiConstants.ACTION_SET_MUSIC_VOLUME);

        if (metrics.heightPixels == 800 ||
            metrics.heightPixels == 744) {
            LinearLayout.LayoutParams soundFxParams = new
                    LinearLayout.LayoutParams(convertDpToPx(141), ViewGroup.LayoutParams.WRAP_CONTENT);
            soundFxParams.gravity = Gravity.CENTER_HORIZONTAL;

            LinearLayout.LayoutParams musicVolumeParams = new
                    LinearLayout.LayoutParams(convertDpToPx(140), ViewGroup.LayoutParams.WRAP_CONTENT);
            musicVolumeParams.gravity = Gravity.CENTER_HORIZONTAL;

            RelativeLayout.LayoutParams bigCircleFirstParams = new
                    RelativeLayout.LayoutParams(convertDpToPx(113), convertDpToPx(113));
            bigCircleFirstParams.setMargins(0, convertDpToPx(66), 0, 0);
            bigCircleFirstParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

            RelativeLayout.LayoutParams smallCircleFirstParams = new
                    RelativeLayout.LayoutParams(convertDpToPx(85), convertDpToPx(85));
            smallCircleFirstParams.setMargins(0, convertDpToPx(80), 0, 0);
            smallCircleFirstParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

            RelativeLayout.LayoutParams bigCircleSecondParams = new
                    RelativeLayout.LayoutParams(convertDpToPx(113), convertDpToPx(113));
            bigCircleSecondParams.setMargins(0, convertDpToPx(66), 0, 0);
            bigCircleSecondParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            bigCircleSecondParams.addRule(RelativeLayout.BELOW, R.id.sound_fx_setting_row);

            RelativeLayout.LayoutParams smallCircleSecondParams = new
                    RelativeLayout.LayoutParams(convertDpToPx(84), convertDpToPx(84));
            smallCircleSecondParams.setMargins(0, convertDpToPx(80), 0, 0);
            smallCircleSecondParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            smallCircleSecondParams.addRule(RelativeLayout.BELOW, R.id.sound_fx_setting_row);

            soundFxVolume.setLayoutParams(soundFxParams);
            musicVolume.setLayoutParams(musicVolumeParams);

            bigCircleFirst.setLayoutParams(bigCircleFirstParams);
            bigCircleSecond.setLayoutParams(bigCircleSecondParams);
            smallCircleFirst.setLayoutParams(smallCircleFirstParams);
            smallCircleSecond.setLayoutParams(smallCircleSecondParams);

            musicVolumeVal.setTextSize(30);
            soundFxVolumeVal.setTextSize(30);
        }
        // -----------------------------------------------------------------------------------

        // Immersive sticky mode to hide and show virtual navigation buttons
        // in required time
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);


        soundFxVolume.setProgress(soundOptions.getSfxVolume());
        musicVolume.setProgress(soundOptions.getMusicVolume());

        checkAndSetSeekbarColor(soundFxVolume, soundFxVolumeVal, soundFxVolume.getProgress());
        checkAndSetSeekbarColor(musicVolume, musicVolumeVal, musicVolume.getProgress());

        soundFxVolumeVal.setText("" + soundFxVolume.getProgress());
        musicVolumeVal.setText("" + musicVolume.getProgress());

        soundFxVolume.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {

            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                soundFxVolumeVal.setText("" + progress);

                checkAndSetSeekbarColor(soundFxVolume, soundFxVolumeVal, progress);

                soundOptions.setSfxVolume(progress);

                Log.d("DATA-UPDATED", "SFX_VOLUME: " + progress);
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {

            }
        });

        musicVolume.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {

            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                musicVolumeVal.setText("" + progress);

                bgMusicServiceIntent.putExtra(ReversiConstants.NEW_MUSIC_VOLUME, progress);
                startService(bgMusicServiceIntent);

                soundOptions.setMusicVolume(progress);

                Log.d("DATA-UPDATED", "MUSIC_VOLUME: " + progress);

                checkAndSetSeekbarColor(musicVolume, musicVolumeVal, progress);
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {

            }
        });

        backToMainMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                reversiApp.setScreen(MainActivity.class.getSimpleName());
                Intent backToMainMenu = new Intent(OptionsActivity.this, MainActivity.class);
                startActivity(backToMainMenu);
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
            }
        });
    }

    private void checkAndSetSeekbarColor(CircularSeekBar seekbar, TextView seekbarVal,
                                         int progress) {
        if (progress > 75) {
            seekbar.setCircleProgressColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.select_color));
            seekbarVal.setTextColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.select_color));
            seekbar.setPointerColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.select_color));
        } else if (progress > 50) {
            seekbar.setCircleProgressColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.md_yellow_600));
            seekbarVal.setTextColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.md_yellow_600));
            seekbar.setPointerColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.md_yellow_600));
        } else if (progress > 25) {
            seekbar.setCircleProgressColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.md_orange_600));
            seekbarVal.setTextColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.md_orange_600));
            seekbar.setPointerColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.md_orange_600));
        } else {
            seekbar.setCircleProgressColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.md_red_900));
            seekbarVal.setTextColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.md_red_900));
            seekbar.setPointerColor(ContextCompat.getColor(OptionsActivity.this,
                    R.color.md_red_900));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (reversiApp.getScreen().equals(OptionsActivity.class.getSimpleName())) {
            Intent pauseBgMusic = new Intent(this, BackgroundMusicService.class);
            pauseBgMusic.setAction(ReversiConstants.ACTION_PAUSE_MUSIC);
            startService(pauseBgMusic);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent playBgMusic = new Intent(this, BackgroundMusicService.class);
        playBgMusic.setAction(ReversiConstants.ACTION_PLAY_MUSIC);
        startService(playBgMusic);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        reversiApp.setScreen(MainActivity.class.getSimpleName());
        Intent backToMainMenu = new Intent(OptionsActivity.this, MainActivity.class);
        startActivity(backToMainMenu);
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        finish();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
    }

    private int convertDpToPx(int dpVal) {
        int pxVal;

        pxVal = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                                                dpVal,
                                                getResources().getDisplayMetrics());

        return pxVal;
    }
}
