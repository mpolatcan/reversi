package com.mpolatcan.reversiapp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import com.crashlytics.android.Crashlytics;
import com.instabug.library.IBGInvocationEvent;
import com.instabug.library.Instabug;
import com.mpolatcan.reversiapp.ReversiBaseApplication;
import com.mpolatcan.reversiapp.services.BackgroundMusicService;
import com.mpolatcan.reversiapp.R;
import com.mpolatcan.reversiapp.services.ReversiDataUpdaterService;
import com.mpolatcan.reversiapp.utils.ReversiConstants;

import io.fabric.sdk.android.Fabric;

/**
 * Created by mpolatcan-gyte_cse on 02.02.2016.
 */
public class MainActivity extends Activity {
    private ImageButton playBtn,
                        optionsBtn,
                        howToPlayBtn,
                        statisticsBtn;
    private Intent bgMusicServiceIntent;
    private Dialog quitGameDialog;
    private ReversiBaseApplication reversiApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("ONCREATE", "<---");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // This code will be deleted in release. That's for performance analysis
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        // ---------------------------------------------------------------------

        Fabric.with(this, new Crashlytics());

        new Instabug.Builder(getApplication(), "d3ed692e0c736d4222d45daa446d7870")
                .setInvocationEvent(IBGInvocationEvent.IBGInvocationEventShake)
                .build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);

        playBtn = (ImageButton) findViewById(R.id.menu_play_btn);
        optionsBtn = (ImageButton) findViewById(R.id.options_btn);
        howToPlayBtn = (ImageButton) findViewById(R.id.how_to_play_btn);
        statisticsBtn = (ImageButton) findViewById(R.id.statistics_btn);

        reversiApp = ReversiBaseApplication.getInstance();

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                reversiApp.setScreen(GameSettingsActivity.class.getSimpleName());
                Intent singlePlayerGame = new Intent(MainActivity.this, GameSettingsActivity.class);
                startActivity(singlePlayerGame);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        });

        optionsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                reversiApp.setScreen(OptionsActivity.class.getSimpleName());
                Intent options = new Intent(MainActivity.this, OptionsActivity.class);
                startActivity(options);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        });

        howToPlayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                reversiApp.setScreen(HowToPlayActivity.class.getSimpleName());
                Intent howToPlay = new Intent(MainActivity.this, HowToPlayActivity.class);
                startActivity(howToPlay);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        });

        statisticsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                reversiApp.setScreen(StatisticsActivity.class.getSimpleName());
                Intent gameStatistics = new Intent(MainActivity.this, StatisticsActivity.class);
                startActivity(gameStatistics);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Instabug.showIntroMessage();
            }
        }, 750);
    }

    @Override
    protected void onStop() {
        Log.d("ONSTOP", "<---");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("ONDESTROY", "<---");
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.d("ONRESUME", "<---");
        super.onResume();
        reversiApp.setScreen(MainActivity.class.getSimpleName());
        Intent playBgMusic = new Intent(this, BackgroundMusicService.class);
        playBgMusic.setAction(ReversiConstants.ACTION_PLAY_MUSIC);
        startService(playBgMusic);
    }

    @Override
    protected void onPause() {
        Log.d("ONPAUSE", "<---");
        super.onPause();
        if (reversiApp.getScreen().equals(MainActivity.class.getSimpleName()) &&
            !reversiApp.isGameClosed()) {
            Intent pauseBgMusic = new Intent(this, BackgroundMusicService.class);
            pauseBgMusic.setAction(ReversiConstants.ACTION_PAUSE_MUSIC);
            startService(pauseBgMusic);
        }
    }

    @Override
    protected void onStart() {
        Log.d("ONSTART", "<---");
        super.onStart();

        new Thread(new Runnable() {
            @Override
            public void run() {
                // Start background music
                bgMusicServiceIntent = new Intent(MainActivity.this, BackgroundMusicService.class);
                bgMusicServiceIntent.setAction(ReversiConstants.ACTION_START);
                startService(bgMusicServiceIntent);
            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        quitGameDialog = new Dialog(this);
        quitGameDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        quitGameDialog.setContentView(R.layout.exit_warning_dialog);
        quitGameDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        quitGameDialog.setCanceledOnTouchOutside(false);

        ImageButton okBtn = (ImageButton) quitGameDialog.findViewById(R.id.warning_dialog_ok_btn);
        ImageButton noBtn = (ImageButton) quitGameDialog.findViewById(R.id.warning_dialog_no_btn);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false,ReversiConstants.CLICK_SOUND);
                reversiApp.setGameClosed(true);

                quitGameDialog.dismiss();

                stopService(bgMusicServiceIntent);

                Intent reversiDataUpdaterIntent = new Intent(MainActivity.this, ReversiDataUpdaterService.class);
                reversiDataUpdaterIntent.setAction(ReversiConstants.ACTION_COMMIT_DATA);
                startService(reversiDataUpdaterIntent);

                finishAffinity();
            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                quitGameDialog.dismiss();
            }
        });

        quitGameDialog.show();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
    }
}
