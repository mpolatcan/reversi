package com.mpolatcan.reversiapp.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.instabug.library.IBGInvocationEvent;
import com.instabug.library.Instabug;
import com.mpolatcan.reversiapp.R;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class SplashActivity extends Activity {
    public Handler handler;
    public Runnable delayRunnable;
    public RelativeLayout developerLayout,
                          poweredLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        developerLayout = ((RelativeLayout) findViewById(R.id.developer_layout));
        poweredLayout = ((RelativeLayout) findViewById(R.id.powered_layout));

        Fabric.with(this, new Crashlytics());

        // Immersive sticky mode to hide and show virtual navigation buttons
        // in required time
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);


        handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                developerLayout.startAnimation(AnimationUtils.loadAnimation(SplashActivity.this,
                        R.anim.text_fade_in));
                developerLayout.setVisibility(View.VISIBLE);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        developerLayout.startAnimation(AnimationUtils.loadAnimation(SplashActivity.this,
                                R.anim.text_fade_out));
                        developerLayout.setVisibility(View.INVISIBLE);

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                poweredLayout.startAnimation(AnimationUtils.loadAnimation(SplashActivity.this,
                                        R.anim.text_fade_in));
                                poweredLayout.setVisibility(View.VISIBLE);
                            }
                        }, 750);
                    }
                }, 2000);
            }
        }, 200);

        delayRunnable = new Runnable() {
            @Override
            public void run() {
                Intent startGame = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(startGame);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
            }
        };
        handler.postDelayed(delayRunnable, 5000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }
}
