package com.mpolatcan.reversiapp.activities;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mpolatcan.reversiapp.R;
import com.mpolatcan.reversiapp.utils.ReversiConstants;

/**
 * Created by mpolatcan-gyte_cse on 12.02.2016.
 */
public class LoadingActivity extends Activity {
    private TextView loadingText;
    private ProgressBar progressBar;
    private Handler handler = new Handler();
    private Runnable progFirst,
                     progSecond,
                     progThird,
                     progFourth;
    private ObjectAnimator animator;
    private int userAvatarId,
                computerAvatarId,
                boardId;
    private String selectedUserAvatarName, selectedComputerAvatarName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        // Immersive sticky mode to hide and show virtual navigation buttons
        // in required time
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);

        userAvatarId = getIntent().getIntExtra(ReversiConstants.SELECTED_USER_AVATAR, -1);
        computerAvatarId = getIntent().getIntExtra(ReversiConstants.SELECTED_CPU_AVATAR, -1);
        boardId = getIntent().getIntExtra(ReversiConstants.SELECTED_BOARD_PATTERN, -1);
        selectedUserAvatarName = getIntent().getStringExtra(ReversiConstants.USER_AVATAR_NAME_DB);
        selectedComputerAvatarName = getIntent().getStringExtra(ReversiConstants.CPU_AVATAR_NAME_DB);

        Log.d("USER_AVATAR-LOADING", "Resource id: " + userAvatarId);
        Log.d("CPU_AVATAR-LOADING", "Resource id: " + computerAvatarId);
        Log.d("BOARD_PATTERN-LOADING", "Resource id: " + boardId);
        Log.d("USR-AVATAR-NAME-LOADING", "Filename: " + selectedUserAvatarName);
        Log.d("CPU-AVATAR-NAME-LOADING", "Filename: " + selectedComputerAvatarName);

        loadingText = (TextView) findViewById(R.id.loading_text);

        progressBar = (ProgressBar) findViewById(R.id.loading_bar);
        progressBar.setMax(10000);
        progressBar.setProgress(0);

        loadingText.setText("Loading %" + progressBar.getProgress());

        progFirst = new Runnable() {
            @Override
            public void run() {
                animator = ObjectAnimator.ofInt(progressBar,
                        "progress", 2500, 4300);
                animator.setDuration(500);
                animator.setInterpolator(new DecelerateInterpolator(1.0f));
                animator.start();
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        loadingText.setText("Loading %" + ((int) animation.getAnimatedValue()) / 100);
                    }
                });

                handler.postDelayed(progSecond, 700);
            }
        };

        progSecond = new Runnable() {
            @Override
            public void run() {
                animator = ObjectAnimator.ofInt(progressBar,
                        "progress", 4300, 6800);
                animator.setDuration(700);
                animator.setInterpolator(new DecelerateInterpolator(1.0f));
                animator.start();
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        loadingText.setText("Loading %" + ((int) animation.getAnimatedValue()) / 100);
                    }
                });

                handler.postDelayed(progThird, 900);
            }
        };

        progThird = new Runnable() {
            @Override
            public void run() {
                animator = ObjectAnimator.ofInt(progressBar,
                        "progress", 6800, 7600);
                animator.setDuration(300);
                animator.setInterpolator(new DecelerateInterpolator(1.0f));
                animator.start();
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        loadingText.setText("Loading %" + ((int) animation.getAnimatedValue()) / 100);
                    }
                });

                handler.postDelayed(progFourth, 450);
            }
        };

        progFourth = new Runnable() {
            @Override
            public void run() {
                animator = ObjectAnimator.ofInt(progressBar,
                        "progress", 7600, 10000);
                animator.setDuration(600);
                animator.setInterpolator(new DecelerateInterpolator(1.0f));
                animator.start();
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        loadingText.setText("Loading %" + ((int) animation.getAnimatedValue()) / 100);
                    }
                });

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent startGame = new Intent(LoadingActivity.this, GameActivity.class);
                        startGame.putExtra(ReversiConstants.SELECTED_USER_AVATAR, userAvatarId);
                        startGame.putExtra(ReversiConstants.SELECTED_CPU_AVATAR, computerAvatarId);
                        startGame.putExtra(ReversiConstants.SELECTED_BOARD_PATTERN, boardId);
                        startGame.putExtra(ReversiConstants.USER_AVATAR_NAME_DB, selectedUserAvatarName);
                        startGame.putExtra(ReversiConstants.CPU_AVATAR_NAME_DB, selectedComputerAvatarName);
                        startActivity(startGame);
                        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                        finish();
                    }
                }, 700);
            }
        };

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animator = ObjectAnimator.ofInt(progressBar,
                        "progress", 0, 2500);
                animator.setDuration(1000);
                animator.setInterpolator(new DecelerateInterpolator(1.0f));
                animator.start();
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        loadingText.setText("Loading %" + ((int) animation.getAnimatedValue()) / 100);
                    }
                });

                handler.postDelayed(progFirst, 1100);
            }
        }, 1000);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
    }
}
