package com.mpolatcan.reversiapp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.crashlytics.android.Crashlytics;
import com.mpolatcan.reversiapp.R;
import com.mpolatcan.reversiapp.ReversiBaseApplication;
import com.mpolatcan.reversiapp.models.UserOptionsModel;
import com.mpolatcan.reversiapp.services.BackgroundMusicService;
import com.mpolatcan.reversiapp.services.ReversiDataUpdaterService;
import com.mpolatcan.reversiapp.utils.ReversiConstants;

import io.fabric.sdk.android.Fabric;
import jp.wasabeef.blurry.Blurry;

public class GameSettingsActivity extends Activity {
    private static int STATE_NUM = 2;

    private ImageButton goToGameBtn,
                        backToMainMenuBtn;
    private ScrollView avatarPanel;
    private TextView unlockedAvatarName;
    private int selectedBackgroundColor,
                availableBackgroundColor,
                lockedBackgroundColor;
    private int selectedBoard, // Resource id of selected board
                selectedUserAvatar, // Resource id of selected user avatar
                selectedComputerAvatar, // Resource id of selected computer avatar
                lastSelectedBoardId, // Resource id of board
                lastSelectedAvatarId, // Position of avatar in avatar array
                avatarNum;
    private String selectedUserAvatarName, selectedComputerAvatarName;
    private TypedArray avatarFilenames,
                       boardFilenames;
    private ImageView unlockedAvatar;
    private Handler handler;
    private RelativeLayout unlockedAvatarSplash,
                           bluredLayout;
    private LinearLayout avatarPanelBase;
    private int[] avatarWinNumbers;
    private DisplayMetrics metrics;
    private ReversiBaseApplication reversiApp;
    private UserOptionsModel userOptions;
    private boolean canTouch = true; // to enable touch event set false else true

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_settings);

        // This code will be deleted in release. That's for performance analysis
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        // ---------------------------------------------------------------------

        Fabric.with(this, new Crashlytics());

        // Immersive sticky mode to hide and show virtual navigation buttons
        // in required time
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }

        goToGameBtn = ((ImageButton) findViewById(R.id.go_to_game_btn));
        avatarPanel = ((ScrollView) findViewById(R.id.avatar_types));
        avatarPanelBase = (LinearLayout) findViewById(R.id.avatar_panel_base);
        backToMainMenuBtn = (ImageButton) findViewById(R.id.settings_back_btn);
        unlockedAvatarSplash = (RelativeLayout) findViewById(R.id.unlocked_avatar_splash);
        unlockedAvatarName = (TextView) findViewById(R.id.unlocked_avatar_name);
        unlockedAvatar = (ImageView) findViewById(R.id.unlocked_avatar);
        bluredLayout = (RelativeLayout) findViewById(R.id.blured_layout);

        // --------------- Setting for WVGA800 screens --------------------------------------
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        if (metrics.heightPixels == 800 ||
            metrics.heightPixels == 744)
            setupBoardPanel(170, 6, 6, 15);
        // -----------------------------------------------------------------------------------

        reversiApp = ReversiBaseApplication.getInstance();
        userOptions = reversiApp.getUserOptions();

        lastSelectedAvatarId = userOptions.getLastSelectedAvatar();
        lastSelectedBoardId = userOptions.getLastSelectedBoard();

        avatarFilenames = getResources().obtainTypedArray(R.array.reversi_avatar_filenames);
        boardFilenames = getResources().obtainTypedArray(R.array.reversi_board_filenames);
        avatarWinNumbers = getResources().getIntArray(R.array.reversi_avatar_win_values);

        selectedBackgroundColor = ContextCompat.getColor(this, R.color.select_color);
        availableBackgroundColor = ContextCompat.getColor(this, R.color.md_black_1100);
        lockedBackgroundColor = ContextCompat.getColor(this, R.color.locked_color);

        // If user doesn't select new avatar or board and press play button immediately get last
        // selection of user
        selectedUserAvatarName = avatarFilenames.getString(lastSelectedAvatarId);

        selectedUserAvatar = getResources().getIdentifier(selectedUserAvatarName,
                                                          "drawable",
                                                           getPackageName());

        selectedComputerAvatarName =
                avatarFilenames.getString(((int) (Math.random() * avatarFilenames.length())));

        selectedComputerAvatar = getResources().getIdentifier(selectedComputerAvatarName,
                                                              "drawable",
                                                              getPackageName());

        while (selectedUserAvatar == selectedComputerAvatar) {
            selectedComputerAvatar = getResources().getIdentifier(
                    avatarFilenames.getString(((int) (Math.random() * avatarFilenames.length()))),
                    "drawable",
                    getPackageName());
        }

        selectedBoard = getResources().getIdentifier(boardFilenames.getString(lastSelectedBoardId),
                                                     "drawable",
                                                     getPackageName());
        // ---------------------------------------------------------------------------------------

        handler = new Handler();

        final int avatarSizeInPx = getResources().getDimensionPixelSize(R.dimen.avatar_size);

        avatarPanel.postDelayed(new Runnable() {
            @Override
            public void run() {
                avatarPanel.scrollTo(0, (lastSelectedAvatarId / 5) * avatarSizeInPx + convertDpToPx(12));
            }
        }, 100);

        goToGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                reversiApp.setScreen(GameActivity.class.getSimpleName());

                // Update user options save last selected avatar and board
                userOptions.setLastSelectedAvatar(lastSelectedAvatarId);
                userOptions.setLastSelectedBoard(lastSelectedBoardId);
                userOptions.setIsAvatarUnlocked(ReversiConstants.DONT_UPDATE);
                userOptions.setUnlockedAvatarIndex(ReversiConstants.DONT_UPDATE);

                Intent goToGame = new Intent(GameSettingsActivity.this, LoadingActivity.class);
                goToGame.putExtra(ReversiConstants.SELECTED_USER_AVATAR, selectedUserAvatar);
                goToGame.putExtra(ReversiConstants.SELECTED_CPU_AVATAR, selectedComputerAvatar);
                goToGame.putExtra(ReversiConstants.SELECTED_BOARD_PATTERN, selectedBoard);
                goToGame.putExtra(ReversiConstants.USER_AVATAR_NAME_DB, selectedUserAvatarName);
                goToGame.putExtra(ReversiConstants.CPU_AVATAR_NAME_DB, selectedComputerAvatarName);
                startActivity(goToGame);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
            }
        });

        backToMainMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                reversiApp.setScreen(MainActivity.class.getSimpleName());

                Intent backToMainMenu = new Intent(GameSettingsActivity.this, MainActivity.class);
                startActivity(backToMainMenu);
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
            }
        });
    }

    private void setupAvatarTypesPanel() {
        final TypedArray avatars = getResources().obtainTypedArray(R.array.reversi_avatars);
        TypedArray avatarNames = getResources().obtainTypedArray(R.array.reversi_avatar_names);

        // Container for put grid rows
        LinearLayout container = ((LinearLayout) findViewById(R.id.avatar_panel_container));

        // Container row for put avatar type previews
        LinearLayout containerRow;
        View avatarPreview, // inflated avatar preview
             verticalBorder, // grid vertical divider
             horizontalBorder; // grid horizontal divider

        containerRow = new LinearLayout(this);
        containerRow.setLayoutParams(
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                              ViewGroup.LayoutParams.WRAP_CONTENT));
        containerRow.setOrientation(LinearLayout.HORIZONTAL);

        int rowSize = avatars.length() / 4;
        avatarNum = avatars.length();

        for (int i = 0; i < avatars.length(); i++) {
            avatarPreview = LayoutInflater.from(this).inflate(R.layout.avatar_type_preview, null);

            ImageButton avatar = ((ImageButton) avatarPreview.findViewById(R.id.avatar));
            RippleView avatarWrapper = ((RippleView) avatarPreview.findViewById(R.id.avatar_ripple));
            ImageView avatarState = ((ImageView) avatarPreview.findViewById(R.id.avatar_state));
            final TextView avatarName = ((TextView) avatarPreview.findViewById(R.id.avatar_name));
            Drawable avatarBackground = avatars.getDrawable(i).getConstantState().newDrawable().mutate();

            /* -------------- Setting for 1280x768 screen --------------------------------- */
            if (metrics.widthPixels == 768) {
                avatar.setLayoutParams(new RelativeLayout.LayoutParams(
                        convertDpToPx(70), convertDpToPx(70)));

                RelativeLayout.LayoutParams avatarNameParams = new RelativeLayout.LayoutParams(
                        convertDpToPx(70), ViewGroup.LayoutParams.WRAP_CONTENT);
                avatarNameParams.addRule(RelativeLayout.BELOW, R.id.avatar_ripple);

                avatarName.setLayoutParams(avatarNameParams);
            }
            /* ----------------------------------------------------------------------------- */
            avatarPreview.setId(i); // Enumerate avatar previews

            // if avatar position is equals to last selected avatar position set this avatar as
            // selected else selected as unlocked or locked
            if (i == lastSelectedAvatarId) {
                avatarPreview.setTag(ReversiConstants.SELECTED_TAG);
                avatarName.setBackgroundColor(selectedBackgroundColor);
                avatarState.setBackgroundColor(Color.TRANSPARENT);
                avatarWrapper.setRippleColor(R.color.select_color);
            } else if (reversiApp.getStatisticsData().getWinNumber() >= avatarWinNumbers[i]) {
                avatarPreview.setTag(ReversiConstants.UNLOCKED_TAG);
                avatarName.setBackgroundColor(availableBackgroundColor);
                avatarState.setBackgroundColor(Color.TRANSPARENT);
                avatarWrapper.setRippleColor(R.color.select_color);
            } else {
                avatarPreview.setTag(ReversiConstants.LOCKED_TAG);
                avatarName.setBackgroundColor(lockedBackgroundColor);
                avatarState.setBackground(ContextCompat.getDrawable(this, R.drawable.lock_icon));
                avatarWrapper.setRippleColor(R.color.locked_color);

                ColorMatrix colorMatrix = new ColorMatrix();
                colorMatrix.setSaturation(0);

                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);

                avatarBackground.setColorFilter(filter);
            }

            // set background corresponding avatar image in that position
            avatar.setBackground(avatarBackground);

            // set avatar's name to text view and setup sliding text and shadows
            avatarName.setText(avatarNames.getString(i));
            avatarName.setSelected(true);

            final int cpyI = i;

            if (avatarPreview.getTag().equals(ReversiConstants.SELECTED_TAG) ||
                avatarPreview.getTag().equals(ReversiConstants.UNLOCKED_TAG)) {
                avatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        reversiApp.playSound(false, ReversiConstants.SELECT_SOUND);

                        Drawable[] states = new Drawable[STATE_NUM];

                        states[0] = avatarName.getBackground();
                        states[1] = new ColorDrawable(selectedBackgroundColor);

                        TransitionDrawable background = new TransitionDrawable(states);
                        background.setCrossFadeEnabled(true);
                        background.startTransition(500);

                        avatarName.setBackground(background);

                        // set tag to define this avatar selected
                        ((View) v.getParent().getParent()).setTag(ReversiConstants.SELECTED_TAG);

                        // save last selected avatar index to save this preference i
                        lastSelectedAvatarId = ((View) v.getParent().getParent()).getId();

                        selectedUserAvatarName = avatarFilenames.getString(lastSelectedAvatarId);

                        selectedUserAvatar = getResources().getIdentifier(
                                avatarFilenames.getString(lastSelectedAvatarId),
                                "drawable",
                                getPackageName());

                        selectedComputerAvatarName =
                                avatarFilenames.getString(((int) (Math.random() * avatarFilenames.length())));

                        selectedComputerAvatar = getResources().getIdentifier(
                                selectedComputerAvatarName,
                                "drawable",
                                getPackageName());

                        while (selectedUserAvatar == selectedComputerAvatar) {
                            selectedComputerAvatar = getResources().getIdentifier(
                                    avatarFilenames.getString(((int) (Math.random() * avatarFilenames.length()))),
                                    "drawable",
                                    getPackageName());
                        }

                        // Remove selected tag from other avatar which was selected
                        for (int j = 0; j < avatarNum; ++j) {
                            if (((View) v.getParent().getParent()).getId() != j &&
                                    findViewById(j).getTag().equals(ReversiConstants.SELECTED_TAG)) {
                                View avatarPreview = findViewById(j);
                                TextView avatarName = ((TextView) avatarPreview.findViewById(R.id.avatar_name));

                                states[0] = avatarName.getBackground();
                                states[1] = new ColorDrawable(availableBackgroundColor);

                                background = new TransitionDrawable(states);
                                background.setCrossFadeEnabled(true);
                                background.startTransition(500);

                                avatarName.setBackground(background);

                                avatarPreview.setTag(ReversiConstants.UNLOCKED_TAG);
                            }
                        }
                    }
                });
            } else {
                avatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog infoDialog = new Dialog(GameSettingsActivity.this);
                        infoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        infoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        infoDialog.setContentView(R.layout.locked_avatar_dialog);
                        infoDialog.setCancelable(true);
                        infoDialog.setCanceledOnTouchOutside(true);

                        ImageView avatar = ((ImageView) infoDialog.findViewById(R.id.info_panel_avatar));
                        ImageButton okBtn = ((ImageButton) infoDialog.findViewById(R.id.avatar_info_ok_btn));
                        TextView avatarName = ((TextView) infoDialog.findViewById(R.id.info_panel_avatar_name));
                        TextView avatarWarn = ((TextView) infoDialog.findViewById(R.id.locked_avatar_warn));
                        Drawable avatarBackground = v.getBackground().getConstantState().newDrawable().mutate();

                        ColorMatrix colorMatrix = new ColorMatrix();
                        colorMatrix.setSaturation(1);

                        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);

                        avatarBackground.setColorFilter(filter);

                        avatar.setBackground(avatarBackground);

                        ViewGroup parent = ((ViewGroup) v.getParent().getParent());

                        avatarName.setText(((TextView) parent.findViewById(R.id.avatar_name)).getText());

                        avatarWarn.setText("You need to win " + avatarWinNumbers[cpyI] + " matches to " +
                                "unlock this avatar!");
                        avatarWarn.setSelected(true);

                        okBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                                infoDialog.dismiss();
                            }
                        });

                        infoDialog.show();
                    }
                });
            }

            /* ----------- Setup dividers of grid layout ---------------------------------- */
            verticalBorder = new View(this);
            horizontalBorder = new View(this);

            verticalBorder.setLayoutParams(
                    new LinearLayout.LayoutParams(convertDpToPx(2), ViewGroup.LayoutParams.MATCH_PARENT));
            verticalBorder.setBackground(
                    new ColorDrawable(ContextCompat.getColor(this, R.color.border_color)));

            horizontalBorder.setLayoutParams(
                    new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, convertDpToPx(2)));
            horizontalBorder.setBackground(
                    new ColorDrawable(ContextCompat.getColor(this, R.color.border_color)));
            /* ---------------------------------------------------------------------------- */

            // Each row has five column so that if 5 modulus of ith position is zero
            // put current row into container because current row is full
            if (i != 0 && i % 5 == 0) {
                // Add current row to container
                container.addView(containerRow);

                // if current row is last row of container don't put horizontal border
                if (container.getChildCount() != (rowSize * 2) - 1)
                    container.addView(horizontalBorder);

                // Instantiate new row
                containerRow = new LinearLayout(this);
                containerRow.setLayoutParams(
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                containerRow.setOrientation(LinearLayout.HORIZONTAL);
                /* --------------------------- */

                // Put current avatar preview and vertical border to new instantiated row
                containerRow.addView(avatarPreview);
                containerRow.addView(verticalBorder);
            } else if (i ==     avatars.length() - 1) {
                // if current row is not full and ith position is last avatar put
                // that avatar and vertical border to current row and then put current
                // row to container immediately
                containerRow.addView(avatarPreview);
                containerRow.addView(verticalBorder);
                container.addView(containerRow);
            } else {
                // Add avatar preview to current row
                containerRow.addView(avatarPreview);

                // If this avatar preview is last column of row don't put vertical border
                if (containerRow.getChildCount() != 9)
                    containerRow.addView(verticalBorder);
            }
        }

        avatars.recycle();
        avatarNames.recycle();
    }

    private void setupBoardTypesPanel() {
        TypedArray boardPreviews = getResources().obtainTypedArray(R.array.reversi_board_previews);
        TypedArray boardNames = getResources().obtainTypedArray(R.array.reversi_board_names);
        LinearLayout boardContainer = ((LinearLayout) findViewById(R.id.board_panel_container));
        
        View boardPreview;

        final int boardNum = boardPreviews.length();

        for (int i = 0; i < boardPreviews.length(); ++i) {
            boardPreview = LayoutInflater.from(this).inflate(R.layout.board_type_preview, null);

            ImageButton board = ((ImageButton) boardPreview.findViewById(R.id.board));
            final TextView boardName = ((TextView) boardPreview.findViewById(R.id.board_name));

            if (metrics.widthPixels == 768) {
                board.setLayoutParams(new RelativeLayout.LayoutParams(
                        convertDpToPx(91), convertDpToPx(91)));

                RelativeLayout.LayoutParams boardNameParams = new RelativeLayout.LayoutParams(
                        convertDpToPx(87), ViewGroup.LayoutParams.WRAP_CONTENT);
                boardNameParams.addRule(RelativeLayout.BELOW, R.id.board_ripple);
                boardNameParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

                boardName.setLayoutParams(boardNameParams);
            }

            boardPreview.setId(i + avatarNum);

            boardName.setText(boardNames.getString(i));

            if (i == lastSelectedBoardId) {
                boardPreview.setTag(ReversiConstants.SELECTED_TAG);
                boardName.setBackgroundColor(ContextCompat.getColor(this, R.color.select_color));
            } else {
                boardPreview.setTag(ReversiConstants.UNLOCKED_TAG);
                boardName.setBackgroundColor(ContextCompat.getColor(this, R.color.md_black_1100));
            }

            board.setBackground(boardPreviews.getDrawable(i));

            board.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reversiApp.playSound(false, ReversiConstants.SELECT_SOUND);

                    Drawable[] states = new Drawable[STATE_NUM];

                    states[0] = boardName.getBackground();
                    states[1] = new ColorDrawable(selectedBackgroundColor);

                    TransitionDrawable background = new TransitionDrawable(states);
                    background.setCrossFadeEnabled(true);
                    background.startTransition(500);

                    boardName.setBackground(background);

                    // set tag to define this board selected
                    ((View) v.getParent().getParent()).setTag(ReversiConstants.SELECTED_TAG);

                    // save last selected avatar index to save this preference i
                    lastSelectedBoardId = ((View) v.getParent().getParent()).getId() - avatarNum;

                    selectedBoard = getResources().getIdentifier(boardFilenames.getString(lastSelectedBoardId),
                            "drawable",
                            getPackageName());

                    // Remove selected tag from other boards which was selected
                    for (int j = 0; j < boardNum; ++j) {
                        if (((View) v.getParent().getParent()).getId() != j + avatarNum &&
                                findViewById(j + avatarNum).getTag().equals(ReversiConstants.SELECTED_TAG))
                        {
                            View boardPreview = findViewById(j + avatarNum);
                            TextView boardName = ((TextView) boardPreview.findViewById(R.id.board_name));

                            states[0] = boardName.getBackground();
                            states[1] = new ColorDrawable(availableBackgroundColor);

                            background = new TransitionDrawable(states);
                            background.setCrossFadeEnabled(true);
                            background.startTransition(500);

                            boardName.setBackground(background);

                            boardPreview.setTag(ReversiConstants.UNLOCKED_TAG);
                        }
                    }
                }
            });

            boardContainer.addView(boardPreview);
        }

        boardPreviews.recycle();
        boardNames.recycle();
    }

    private int convertDpToPx(int dpVal) {
        int pxVal;

        pxVal = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                                                dpVal,
                                                getResources().getDisplayMetrics());

        return pxVal;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        reversiApp.setScreen(MainActivity.class.getSimpleName());
        Intent backToMainMenu = new Intent(GameSettingsActivity.this, MainActivity.class);
        startActivity(backToMainMenu);
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        finish();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (reversiApp.getScreen().equals(GameSettingsActivity.class.getSimpleName())) {
            Intent pauseBgMusic = new Intent(this, BackgroundMusicService.class);
            pauseBgMusic.setAction(ReversiConstants.ACTION_PAUSE_MUSIC);
            startService(pauseBgMusic);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent playBgMusic = new Intent(this, BackgroundMusicService.class);
        playBgMusic.setAction(ReversiConstants.ACTION_PLAY_MUSIC);
        startService(playBgMusic);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return canTouch && super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onStart() {
        super.onStart();
        new Thread(new Runnable() {
            @Override
            public void run() {

                final Animation fadeInAnim = AnimationUtils.loadAnimation(GameSettingsActivity.this,
                        R.anim.text_fade_in);
                final Animation fadeOutAnim = AnimationUtils.loadAnimation(GameSettingsActivity.this,
                        R.anim.text_fade_out);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setupAvatarTypesPanel();

                        setupBoardTypesPanel();

                        fadeInAnim.setDuration(300);
                        fadeOutAnim.setDuration(300);

                        Log.d("UNLOCKED", "" +
                                (userOptions.getIsAvatarUnlocked() == ReversiConstants.UNLOCKED ? "UNLOCKED" : "NOT_UNLOCKED"));
                        if (userOptions.getIsAvatarUnlocked() == ReversiConstants.UNLOCKED)
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Blurry.with(GameSettingsActivity.this)
                                            .radius(20)
                                            .sampling(2)
                                            .color(Color.argb(150, 0, 0, 0))
                                            .onto(bluredLayout);

                                    final Intent bgMusicServiceIntent = new Intent(GameSettingsActivity.this,
                                            ReversiDataUpdaterService.class);

                                    int unlockedAvatarIndex = userOptions.getUnlockedAvatarIndex();

                                    unlockedAvatarName.setText(
                                            getResources().obtainTypedArray(R.array.reversi_avatar_names)
                                                    .getString(unlockedAvatarIndex));
                                    unlockedAvatar.setBackground(
                                            getResources().obtainTypedArray(R.array.reversi_avatars)
                                                    .getDrawable(unlockedAvatarIndex));

                                    // Pause background music and start unlocked sound
                                    bgMusicServiceIntent.setAction(ReversiConstants.ACTION_PAUSE_MUSIC);
                                    startService(bgMusicServiceIntent);
                                    reversiApp.playSound(false, ReversiConstants.UNLOCKED_SOUND);

                                    unlockedAvatarSplash.startAnimation(fadeInAnim);
                                    unlockedAvatarSplash.setVisibility(View.VISIBLE);
                                    canTouch = false;

                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Blurry.delete(bluredLayout);

                                            unlockedAvatarSplash.startAnimation(fadeOutAnim);
                                            unlockedAvatarSplash.setVisibility(View.INVISIBLE);

                                            // Start again background music
                                            bgMusicServiceIntent.setAction(ReversiConstants.ACTION_PLAY_MUSIC);
                                            startService(bgMusicServiceIntent);

                                            userOptions.setLastSelectedAvatar(userOptions.getLastSelectedAvatar());
                                            userOptions.setLastSelectedBoard(userOptions.getLastSelectedBoard());
                                            userOptions.setIsAvatarUnlocked(ReversiConstants.NOT_UNLOCKED);
                                            userOptions.setUnlockedAvatarIndex(ReversiConstants.DONT_UPDATE);

                                            canTouch = true;
                                        }
                                    }, 2000);
                                }
                            }, 1000);
                    }
                }, 300);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                         findViewById(R.id.game_setting_progress).startAnimation(fadeOutAnim);
                         findViewById(R.id.game_setting_progress).setVisibility(View.INVISIBLE);

                         handler.postDelayed(new Runnable() {
                             @Override
                             public void run() {
                                 bluredLayout.startAnimation(fadeInAnim);
                                 bluredLayout.setVisibility(View.VISIBLE);
                             }
                         }, 500);
                    }
                }, 2000);

            }
        }).start();
    }

    private void setupBoardPanel(int heightDp, int marginLeftDp, int marginRightDp,
                                int marginBottomDp) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, convertDpToPx(heightDp));
        params.setMargins(convertDpToPx(marginLeftDp), 0, convertDpToPx(marginRightDp), convertDpToPx(marginBottomDp));
        params.addRule(RelativeLayout.BELOW, R.id.choose_avatar_label);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        avatarPanelBase.setLayoutParams(params);
    }
}
