package com.mpolatcan.reversiapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.mpolatcan.reversiapp.R;
import com.mpolatcan.reversiapp.ReversiBaseApplication;
import com.mpolatcan.reversiapp.models.StatisticsDataModel;
import com.mpolatcan.reversiapp.services.BackgroundMusicService;
import com.mpolatcan.reversiapp.utils.ReversiConstants;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;

public class StatisticsActivity extends Activity {
    private Handler handler;
    private Runnable winNumRun,
                     lostNumRun,
                     highScoreNumRun,
                     mostBeatenAvatarRun,
                     throneSlideRun,
                     crownSlideRun,
                     avatarFadeInRun;
    private ImageButton backToMainMenu;
    private ScheduledExecutorService executor;
    private int i,
                winNumberSpeed,
                lostNumberSpeed;
    private ReversiBaseApplication reversiApp;
    private StatisticsDataModel statisticsData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        // This code will be deleted in release. That's for performance analysis
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        // ---------------------------------------------------------------------

        Fabric.with(this, new Crashlytics());

        executor = Executors.newSingleThreadScheduledExecutor();

        backToMainMenu = (ImageButton) findViewById(R.id.statistics_back_btn);

        reversiApp = ReversiBaseApplication.getInstance();
        statisticsData = reversiApp.getStatisticsData();

        // Immersive sticky mode to hide and show virtual navigation buttons
        // in required time
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);

        handler = new Handler();

        i = 0;

        backToMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                reversiApp.setScreen(MainActivity.class.getSimpleName());
                Intent backToMainMenu = new Intent(StatisticsActivity.this, MainActivity.class);
                startActivity(backToMainMenu);
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
            }
        });


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setupStatisticsPanel();
            }
        }, 750);
    }

    private void setupStatisticsPanel() {
        final TextView winNumber = ((TextView) findViewById(R.id.win_number));
        final TextView lostNumber = ((TextView) findViewById(R.id.lost_number));
        final TextView highScore = ((TextView) findViewById(R.id.high_score));
        final ImageView mostPlayedAvatar = ((ImageView) findViewById(R.id.most_played_avatar));
        final ImageView mostBeatenAvatar = ((ImageView) findViewById(R.id.most_beaten_avatar));

        final int statWinNum = statisticsData.getWinNumber();
        final int statLostNum = statisticsData.getLostNumber();
        final int statHighScore = statisticsData.getHighScore();

        getWinNumSpeed(statWinNum);
        getLostNumSpeed(statLostNum);

        winNumRun = new Runnable() {
            @Override
            public void run() {
                executor.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {
                        if (i <= statWinNum) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    winNumber.setText("" + i);
                                    ++i;
                                }
                            });
                        } else {
                            executor.shutdown();
                            handler.postDelayed(lostNumRun, 100);
                        }
                    }
                }, 0, winNumberSpeed, TimeUnit.MILLISECONDS);
            }
        };

        lostNumRun = new Runnable() {
            @Override
            public void run() {
                i = 0;
                executor = Executors.newSingleThreadScheduledExecutor();
                executor.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {
                        if (i <= statLostNum) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    lostNumber.setText("" + i);
                                    ++i;
                                }
                            });
                        } else {
                            executor.shutdown();
                            handler.postDelayed(highScoreNumRun, 100);
                        }
                    }
                }, 0, lostNumberSpeed, TimeUnit.MILLISECONDS);
            }
        };

        highScoreNumRun = new Runnable() {
            @Override
            public void run() {
                i = 0;
                executor = Executors.newSingleThreadScheduledExecutor();
                executor.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {
                        if (i <= statHighScore) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    highScore.setText("" + i);
                                }
                            });
                            ++i;
                        } else {
                            executor.shutdown();
                            handler.postDelayed(mostBeatenAvatarRun, 100);
                        }
                    }
                }, 0, 50, TimeUnit.MILLISECONDS);

            }
        };

        mostBeatenAvatarRun = new Runnable() {
            @Override
            public void run() {
                executor.shutdown();
                mostBeatenAvatar.setAnimation(AnimationUtils.loadAnimation(StatisticsActivity.this,
                        R.anim.text_fade_in));
                mostBeatenAvatar.setVisibility(View.VISIBLE);
            }
        };

        if (statisticsData.getMostPlayedAvatar() == -1 ||
            statisticsData.getMostBeatenAvatar() == -1) {
            mostPlayedAvatar.setBackground(new ColorDrawable(Color.TRANSPARENT));
            mostBeatenAvatar.setBackground(new ColorDrawable(Color.TRANSPARENT));

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ImageView mostPlayedThrone = ((ImageView) findViewById(R.id.most_played_throne));
                    mostPlayedThrone.startAnimation(
                            AnimationUtils.loadAnimation(StatisticsActivity.this, R.anim.text_fade_in));
                    mostPlayedThrone.setVisibility(View.VISIBLE);
                }
            }, 300);

            handler.postDelayed(winNumRun, 900);
        } else {
            final ImageView throne = ((ImageView) findViewById(R.id.most_played_throne));
            final ImageView crown = ((ImageView) findViewById(R.id.statistics_crown));

            mostPlayedAvatar.setImageDrawable(ContextCompat.getDrawable(this,
                    statisticsData.getMostPlayedAvatar()));
            mostPlayedAvatar.setVisibility(View.INVISIBLE);

            throne.setVisibility(View.INVISIBLE);

            throneSlideRun = new Runnable() {
                @Override
                public void run() {
                    Animation throneSlide = AnimationUtils.loadAnimation(StatisticsActivity.this,
                            R.anim.left_slide_in);
                    throneSlide.setDuration(500);

                    throne.startAnimation(throneSlide);
                    throne.setVisibility(View.VISIBLE);

                    handler.postDelayed(avatarFadeInRun, 750);
                }
            };

            avatarFadeInRun = new Runnable() {
                @Override
                public void run() {
                    mostPlayedAvatar.startAnimation(
                            AnimationUtils.loadAnimation(StatisticsActivity.this, R.anim.text_fade_in));
                    mostPlayedAvatar.setVisibility(View.VISIBLE);

                    handler.postDelayed(crownSlideRun, 750);
                }
            };

            crownSlideRun = new Runnable() {
                @Override
                public void run() {
                    Animation crownSlide = AnimationUtils.loadAnimation(StatisticsActivity.this,
                            R.anim.down_slide);
                    crownSlide.setDuration(300);

                    crown.startAnimation(crownSlide);
                    crown.setVisibility(View.VISIBLE);

                    handler.postDelayed(winNumRun, 750);
                }
            };

            handler.postDelayed(throneSlideRun, 50);

            mostBeatenAvatar.setImageDrawable(ContextCompat.getDrawable(this,
                    statisticsData.getMostBeatenAvatar()));
            mostBeatenAvatar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        reversiApp.setScreen(MainActivity.class.getSimpleName());
        Intent backToMainMenu = new Intent(this, MainActivity.class);
        startActivity(backToMainMenu);
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (reversiApp.getScreen().equals(StatisticsActivity.class.getSimpleName())) {
            Intent pauseBgMusic = new Intent(this, BackgroundMusicService.class);
            pauseBgMusic.setAction(ReversiConstants.ACTION_PAUSE_MUSIC);
            startService(pauseBgMusic);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent startBgMusic = new Intent(this, BackgroundMusicService.class);
        startBgMusic.setAction(ReversiConstants.ACTION_PLAY_MUSIC);
        startService(startBgMusic);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }

        super.onWindowFocusChanged(hasFocus);
    }

    public void getWinNumSpeed(int val) {
        if (val <= 50)
            winNumberSpeed = 50;
        else if (val > 50 && val <= 100)
            winNumberSpeed = 40;
        else if (val > 100 && val <= 200)
            winNumberSpeed = 30;
        else if (val > 200 && val <= 300)
            winNumberSpeed = 20;
        else if (val > 300 && val <= 400)
            winNumberSpeed = 10;
        else
            winNumberSpeed = 5;
    }

    public void getLostNumSpeed(int val) {
        if (val <= 50)
            lostNumberSpeed = 50;
        else if (val > 50 && val <= 100)
            lostNumberSpeed = 40;
        else if (val > 100 && val <= 200)
            lostNumberSpeed = 30;
        else if (val > 200 && val <= 300)
            lostNumberSpeed = 20;
        else if (val > 300 && val <= 400)
            lostNumberSpeed = 10;
        else
            lostNumberSpeed = 5;
    }
}
