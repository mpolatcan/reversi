package com.mpolatcan.reversiapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.mpolatcan.reversiapp.R;
import com.mpolatcan.reversiapp.ReversiBaseApplication;
import com.mpolatcan.reversiapp.services.BackgroundMusicService;
import com.mpolatcan.reversiapp.utils.ReversiConstants;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;

public class HowToPlayActivity extends Activity {
    private ImageView boardSlideshow,
                      firstPlayerSlideShow,
                      secondPlayerSlideShow,
                      moveSlideshow,
                      comboSlideshow;
    private ImageButton backToMainMenuBtn;
    private Handler handler;
    private TypedArray boards,
                       avatars;
    private ScheduledExecutorService boardSlideshowExecutor,
                                     firstPlayerAvatarSlideshowExecutor,
                                     secondPlayerAvatarSlideshowExecutor,
                                     moveSlideshowExecutor,
                                     comboSlideshowExecutor;
    private int boardIndex = 1,
                moveIndex = 1,
                comboIndex = 1;
    private Drawable[] moveStates,
                       comboStates;
    private ReversiBaseApplication reversiApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_play);

        // This code will be deleted in release. That's for performance analysis
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        // ---------------------------------------------------------------------

        Fabric.with(this, new Crashlytics());

        // Immersive sticky mode to hide and show virtual navigation buttons
        // in required time
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);

        boardSlideshow = (ImageView) findViewById(R.id.board_slideshow);
        firstPlayerSlideShow = (ImageView) findViewById(R.id.first_player_avatar);
        secondPlayerSlideShow = (ImageView) findViewById(R.id.second_player_avatar);
        moveSlideshow = (ImageView) findViewById(R.id.move_slideshow);
        comboSlideshow = (ImageView) findViewById(R.id.combo_slideshow);
        backToMainMenuBtn = (ImageButton) findViewById(R.id.how_to_play_back_btn);

        reversiApp = ReversiBaseApplication.getInstance();

        boards = getResources().obtainTypedArray(R.array.reversi_board_previews);
        avatars = getResources().obtainTypedArray(R.array.reversi_avatars);

        moveStates = new Drawable[2];
        moveStates[0] = ContextCompat.getDrawable(this, R.drawable.reversi_initial_state);
        moveStates[1] = ContextCompat.getDrawable(this, R.drawable.reversi_after_move);

        comboStates = new Drawable[2];
        comboStates[0] = ContextCompat.getDrawable(this, R.drawable.reversi_before_combo);
        comboStates[1] = ContextCompat.getDrawable(this, R.drawable.reversi_after_combo);

        handler = new Handler();

        boardSlideshowExecutor = Executors.newSingleThreadScheduledExecutor();
        firstPlayerAvatarSlideshowExecutor = Executors.newSingleThreadScheduledExecutor();
        secondPlayerAvatarSlideshowExecutor = Executors.newSingleThreadScheduledExecutor();
        moveSlideshowExecutor = Executors.newSingleThreadScheduledExecutor();
        comboSlideshowExecutor = Executors.newSingleThreadScheduledExecutor();

        backToMainMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(false, ReversiConstants.CLICK_SOUND);
                reversiApp.setScreen(MainActivity.class.getSimpleName());
                Intent backToMainMenu = new Intent(HowToPlayActivity.this, MainActivity.class);
                startActivity(backToMainMenu);
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        reversiApp.setScreen(MainActivity.class.getSimpleName());
        Intent backToMainMenu = new Intent(this, MainActivity.class);
        startActivity(backToMainMenu);
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        boardSlideshowExecutor.shutdown();
        firstPlayerAvatarSlideshowExecutor.shutdown();
        secondPlayerAvatarSlideshowExecutor.shutdown();
        moveSlideshowExecutor.shutdown();
        comboSlideshowExecutor.shutdown();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }

        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (reversiApp.getScreen().equals(HowToPlayActivity.class.getSimpleName())) {
            Intent pauseBgMusic = new Intent(this, BackgroundMusicService.class);
            pauseBgMusic.setAction(ReversiConstants.ACTION_PAUSE_MUSIC);
            startService(pauseBgMusic);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent playBgMusic = new Intent(this, BackgroundMusicService.class);
        playBgMusic.setAction(ReversiConstants.ACTION_PLAY_MUSIC);
        startService(playBgMusic);
    }

    @Override
    protected void onStart() {
        super.onStart();

        boardSlideshowExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (boardIndex == boards.length())
                            boardIndex = 0;

                        Drawable[] states = new Drawable[2];
                        states[0] = boardSlideshow.getBackground();
                        states[1] = boards.getDrawable(boardIndex);

                        TransitionDrawable slideshow = new TransitionDrawable(states);

                        slideshow.setCrossFadeEnabled(true);
                        slideshow.startTransition(750);
                        boardSlideshow.setBackground(slideshow);

                        ++boardIndex;
                    }
                });
            }
        }, 1, 3, TimeUnit.SECONDS);

        firstPlayerAvatarSlideshowExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Drawable[] states = new Drawable[2];
                        states[0] = firstPlayerSlideShow.getBackground();
                        states[1] = avatars.getDrawable(((int) (Math.random() * avatars.length())));

                        TransitionDrawable slideshow = new TransitionDrawable(states);

                        slideshow.setCrossFadeEnabled(true);
                        slideshow.startTransition(750);
                        firstPlayerSlideShow.setBackground(slideshow);
                    }
                });
            }
        }, 1, 5, TimeUnit.SECONDS);

        secondPlayerAvatarSlideshowExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Drawable[] states = new Drawable[2];
                        states[0] = secondPlayerSlideShow.getBackground();
                        states[1] = avatars.getDrawable(((int) (Math.random() * avatars.length())));

                        TransitionDrawable slideshow = new TransitionDrawable(states);

                        slideshow.setCrossFadeEnabled(true);
                        slideshow.startTransition(750);
                        secondPlayerSlideShow.setBackground(slideshow);
                    }
                });
            }
        }, 1, 7, TimeUnit.SECONDS);

        moveSlideshowExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (moveIndex == 2)
                            moveIndex = 0;

                        Drawable[] states = new Drawable[2];
                        states[0] = moveSlideshow.getBackground();
                        states[1] = moveStates[moveIndex];

                        TransitionDrawable slideshow = new TransitionDrawable(states);

                        slideshow.setCrossFadeEnabled(true);
                        slideshow.startTransition(750);
                        moveSlideshow.setBackground(slideshow);

                        ++moveIndex;
                    }
                });
            }
        }, 1, 2, TimeUnit.SECONDS);

        comboSlideshowExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (comboIndex == 2)
                            comboIndex = 0;

                        Drawable[] states = new Drawable[2];
                        states[0] = comboSlideshow.getBackground();
                        states[1] = comboStates[comboIndex];

                        TransitionDrawable slideshow = new TransitionDrawable(states);

                        slideshow.setCrossFadeEnabled(true);
                        slideshow.startTransition(750);
                        comboSlideshow.setBackground(slideshow);

                        ++comboIndex;
                    }
                });
            }
        }, 1, 4, TimeUnit.SECONDS);
    }
}
