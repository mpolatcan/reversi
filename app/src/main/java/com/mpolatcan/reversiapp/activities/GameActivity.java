package com.mpolatcan.reversiapp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import com.andexert.library.RippleView;
import com.crashlytics.android.Crashlytics;
import com.mpolatcan.reversiapp.ReversiBaseApplication;
import com.mpolatcan.reversiapp.logic.Piece;
import com.mpolatcan.reversiapp.R;
import com.mpolatcan.reversiapp.services.BackgroundMusicService;
import com.mpolatcan.reversiapp.services.ReversiDataUpdaterService;
import com.mpolatcan.reversiapp.utils.ReversiConstants;
import com.mpolatcan.reversiapp.logic.ReversiGame;
import com.mpolatcan.reversiapp.customviews.ReversiTextView;
import io.fabric.sdk.android.Fabric;

// TODO Firebase App Indexing integration
// TODO Publish app in Google Play Store

public class GameActivity extends Activity {
    private ReversiGame game;
    public static TableLayout board;
    private ImageButton pauseBtn,
                        settingsBtn,
                        settingsSoundBtn,
                        settingsMusicBtn,
                        settingsAboutBtn;
    private ImageView userAvatar,
                      computerAvatar,
                      preGameUserAvatar,
                      preGameCpuAvatar;
    private RelativeLayout preGameSplash;
    private LinearLayout settingsPanel;
    private TextSwitcher userScoreboard,
                         computerScoreboard;
    private RelativeLayout.LayoutParams layoutParams;
    private Dialog pauseDialog,
                   warningDialog,
                   aboutDialog,
                   resultDialog;
    private String selectedUserAvatarName,
                   selectedComputerAvatarName;
    private Vibrator wrongMoveVibrator;
    private Handler handler;
    private int userAvatarId,
                computerAvatarId,
                boardId;
    private ReversiBaseApplication reversiApp;
    private Intent bgMusicServiceIntent;
    public boolean soundMuted = false;
    public boolean yourTurn = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // This code will be deleted in release. That's for performance analysis
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        // ---------------------------------------------------------------------

        Fabric.with(this, new Crashlytics());

        // Immersive sticky mode to hide and show virtual navigation buttons
        // in required time
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);

        int phoneWidth;

        // Get current phone's width in pixels
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        userAvatarId = getIntent().getIntExtra(ReversiConstants.SELECTED_USER_AVATAR, -1);
        computerAvatarId = getIntent().getIntExtra(ReversiConstants.SELECTED_CPU_AVATAR, -1);
        boardId = getIntent().getIntExtra(ReversiConstants.SELECTED_BOARD_PATTERN, -1);
        selectedUserAvatarName = getIntent().getStringExtra(ReversiConstants.USER_AVATAR_NAME_DB);
        selectedComputerAvatarName = getIntent().getStringExtra(ReversiConstants.CPU_AVATAR_NAME_DB);

        Log.d("USER_AVATAR-GAME", "Resource id: " + userAvatarId);
        Log.d("CPU_AVATAR-GAME", "Resource id: " + computerAvatarId);
        Log.d("BOARD_PATTERN-GAME", "Resource id: " + boardId);
        Log.d("USER-AVATAR-NAME-GAME", "Filename: " + selectedUserAvatarName);
        Log.d("CPU-AVATAR-NAME-GAME", "Filename " + selectedComputerAvatarName);

        phoneWidth = metrics.widthPixels;

        // --------------------------------------------------------
        board = (TableLayout) findViewById(R.id.board);
        userScoreboard = (TextSwitcher) findViewById(R.id.first_player_scoreboard);
        computerScoreboard = (TextSwitcher) findViewById(R.id.second_player_scoreboard);
        pauseBtn = (ImageButton) findViewById(R.id.pause_btn);
        settingsBtn = (ImageButton) findViewById(R.id.settings_btn);
        settingsPanel = (LinearLayout) findViewById(R.id.settings_panel);
        settingsSoundBtn = (ImageButton) findViewById(R.id.settings_sound_btn);
        settingsMusicBtn = (ImageButton) findViewById(R.id.settings_music_btn);
        settingsAboutBtn = (ImageButton) findViewById(R.id.settings_info_btn);
        userAvatar = (ImageView) findViewById(R.id.user_avatar);
        computerAvatar = (ImageView) findViewById(R.id.computer_avatar);
        preGameUserAvatar = (ImageView) findViewById(R.id.versus_user_avatar);
        preGameCpuAvatar = (ImageView) findViewById(R.id.versus_cpu_avatar);
        preGameSplash = (RelativeLayout) findViewById(R.id.pre_game_splash);

        preGameUserAvatar.setImageDrawable(ContextCompat.getDrawable(this, userAvatarId));
        preGameCpuAvatar.setImageDrawable(ContextCompat.getDrawable(this, computerAvatarId));

        wrongMoveVibrator = ((Vibrator) getSystemService(VIBRATOR_SERVICE));
        
        // Scale layout according to phone width
        layoutParams = new RelativeLayout.LayoutParams(phoneWidth, phoneWidth);
        layoutParams.addRule(RelativeLayout.BELOW, R.id.reversi_game_logo);
        layoutParams.setMargins(0, convertDpToPx(40), 0, 0);

        board.setLayoutParams(layoutParams);
        // ----------------------------------------------------------------------------------------

        // Scale background according to phone width
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), boardId);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, phoneWidth, phoneWidth, true);
        Drawable background = new BitmapDrawable(getResources(), scaledBitmap);
        board.setBackground(background);
        // ----------------------------------------------------------------------------------------

        setupBoard(); // Configure initial board
        setupPauseDialog(); // Configure pause menu
        setupResultDialog(); // Configure result dialog
        setupScoreboards(); // Configure score board
        setupSettingsPanel(); // Configure settings panel
        setupAboutDialog(); // Configure about dialog

        game = new ReversiGame(board, this);
        reversiApp = ReversiBaseApplication.getInstance();

        bgMusicServiceIntent = new Intent(this, BackgroundMusicService.class);

        final Animation upSlideAnim = AnimationUtils.loadAnimation(this, R.anim.up_slide);
        final Animation downSlideAnim = AnimationUtils.loadAnimation(this, R.anim.down_slide);

        pauseBtn.setVisibility(View.INVISIBLE);
        settingsBtn.setVisibility(View.INVISIBLE);
        userScoreboard.setVisibility(View.INVISIBLE);
        computerScoreboard.setVisibility(View.INVISIBLE);
        userAvatar.setVisibility(View.INVISIBLE);
        computerAvatar.setVisibility(View.INVISIBLE);
        findViewById(R.id.reversi_game_logo).setVisibility(View.INVISIBLE);

        handler = new Handler();

        final Runnable gameMainGui = new Runnable() {
            @Override
            public void run() {
                userScoreboard.startAnimation(upSlideAnim);
                computerScoreboard.startAnimation(upSlideAnim);

                userAvatar.startAnimation(upSlideAnim);
                computerAvatar.startAnimation(upSlideAnim);

                pauseBtn.startAnimation(downSlideAnim);
                settingsBtn.startAnimation(downSlideAnim);
                findViewById(R.id.reversi_game_logo).startAnimation(downSlideAnim);
            }
        };

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final Animation leftSlideIn = AnimationUtils.loadAnimation(GameActivity.this,
                        R.anim.left_slide_in);
                final Animation rightSlideIn = AnimationUtils.loadAnimation(GameActivity.this,
                        R.anim.right_slide_in);

                leftSlideIn.setDuration(500);
                rightSlideIn.setDuration(500);

                preGameUserAvatar.startAnimation(leftSlideIn);
                preGameUserAvatar.setVisibility(View.VISIBLE);

                preGameCpuAvatar.setAnimation(rightSlideIn);
                preGameCpuAvatar.setVisibility(View.VISIBLE);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        preGameSplash.startAnimation(
                                AnimationUtils.loadAnimation(GameActivity.this, R.anim.text_fade_out));
                        preGameSplash.setVisibility(View.GONE);
                    }
                }, 1500);

                handler.postDelayed(gameMainGui, 2000);
            }
        }, 750);

        game.moveGuide();
    }

    @Override
    public void onBackPressed() {
        pauseDialog.show();
    }

    private void setupBoard() {
        for (int i = 0; i < board.getChildCount(); ++i) {
            TableRow row = ((TableRow) board.getChildAt(i));

            for (int j = 0; j < row.getChildCount(); ++j) {
                ((RippleView) row.getChildAt(j)).getChildAt(0)
                        .setTag(new Piece(j, i, ReversiConstants.NONE));
                ((ImageView) ((RippleView) row.getChildAt(j)).getChildAt(0)).setImageResource(R.drawable.transparent_bg);
                ((RippleView) row.getChildAt(j)).getChildAt(0).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (settingsPanel.getVisibility() == View.VISIBLE) {
                                    settingsPanel.startAnimation(AnimationUtils.loadAnimation(GameActivity.this, R.anim.scale_dec_panel));
                                    settingsPanel.setVisibility(View.INVISIBLE);
                                }

                                if (yourTurn) {
                                    if (((Piece) v.getTag()).getOwner() == ReversiConstants.NONE)
                                        game.play(((Piece) v.getTag()));
                                    else
                                        wrongMoveVibrator.vibrate(50);
                                }
                            }
                        });
            }
        }

        TableRow fourthRow, fifthRow;
        ImageView piece;

        int FOURTH_ROW_OR_COL = (ReversiConstants.BOARD_SIZE / 2) - 1;
        int FIFTH_ROW_OR_COL = ReversiConstants.BOARD_SIZE / 2;

        fourthRow = ((TableRow) board.getChildAt(FOURTH_ROW_OR_COL));
        fifthRow = ((TableRow) board.getChildAt(FIFTH_ROW_OR_COL));

        piece = ((ImageView) ((RippleView) fourthRow.getChildAt(FOURTH_ROW_OR_COL)).getChildAt(0));
        piece.setTag(new Piece(FOURTH_ROW_OR_COL, FOURTH_ROW_OR_COL, ReversiConstants.USER));
        piece.setImageResource(userAvatarId);

        piece = ((ImageView) ((RippleView) fifthRow.getChildAt(FIFTH_ROW_OR_COL)).getChildAt(0));
        piece.setTag(new Piece(FIFTH_ROW_OR_COL, FIFTH_ROW_OR_COL, ReversiConstants.USER));
        piece.setImageResource(userAvatarId);

        piece = ((ImageView) ((RippleView) fourthRow.getChildAt(FIFTH_ROW_OR_COL)).getChildAt(0));
        piece.setTag(new Piece(FIFTH_ROW_OR_COL, FOURTH_ROW_OR_COL, ReversiConstants.COMPUTER));
        piece.setImageResource(computerAvatarId);

        piece = ((ImageView) ((RippleView) fifthRow.getChildAt(FOURTH_ROW_OR_COL)).getChildAt(0));
        piece.setTag(new Piece(FOURTH_ROW_OR_COL, FIFTH_ROW_OR_COL, ReversiConstants.COMPUTER));
        piece.setImageResource(computerAvatarId);

    }

    private void restartGame() {
        userScoreboard.setText("0");
        computerScoreboard.setText("0");

        setupBoard();

        game = new ReversiGame(board, this);
        yourTurn = true;

        game.moveGuide();
    }

    private void setupPauseDialog() {
        pauseDialog = new Dialog(this);
        pauseDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pauseDialog.setContentView(R.layout.pause_menu_dialog);
        pauseDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pauseDialog.setCanceledOnTouchOutside(true);
        pauseDialog.setCancelable(true);

        ImageButton resumeBtn = (ImageButton) pauseDialog.findViewById(R.id.pause_dialog_resume_btn);
        ImageButton restartBtn = (ImageButton) pauseDialog.findViewById(R.id.pause_dialog_restart_btn);
        ImageButton mainMenuBtn = (ImageButton) pauseDialog.findViewById(R.id.pause_dialog_main_menu_btn);

        resumeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);
                pauseDialog.dismiss();
            }
        });

        restartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);
                restartGame();
                pauseDialog.dismiss();
            }
        });


        mainMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);

                warningDialog = new Dialog(GameActivity.this);
                warningDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                warningDialog.setContentView(R.layout.exit_warning_dialog);
                warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                warningDialog.setCanceledOnTouchOutside(false);

                ImageButton okBtn = (ImageButton) warningDialog.findViewById(R.id.warning_dialog_ok_btn);
                ImageButton noBtn = (ImageButton) warningDialog.findViewById(R.id.warning_dialog_no_btn);

                okBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);
                        reversiApp.setScreen(MainActivity.class.getSimpleName());

                        warningDialog.dismiss();

                        Intent goToMainMenu = new Intent(GameActivity.this, MainActivity.class);
                        startActivity(goToMainMenu);
                        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                        finish();
                    }
                });

                noBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);
                        warningDialog.dismiss();
                    }
                });

                pauseDialog.dismiss();
                warningDialog.show();
            }
        });

        pauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);
                pauseDialog.show();
            }
        });
    }

    private void setupAboutDialog() {
        aboutDialog = new Dialog(this);
        aboutDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        aboutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        aboutDialog.setContentView(R.layout.about_dialog);
        aboutDialog.setCanceledOnTouchOutside(true);
        aboutDialog.setCancelable(true);

        final ViewFlipper slider = (ViewFlipper) aboutDialog.findViewById(R.id.about_slider);

        slider.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.right_slide_in));
        slider.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.left_slide_out));

        aboutDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                slider.startFlipping();
            }
        });

        aboutDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                slider.stopFlipping();
            }
        });
    }

    private void setupScoreboards() {
        userScoreboard.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ReversiTextView scoreboard = new ReversiTextView(GameActivity.this);
                scoreboard.setPadding(0, convertDpToPx(2), convertDpToPx(20), 0);
                scoreboard.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                scoreboard.setTextSize(25);
                scoreboard.setText("0");
                return scoreboard;
            }
        });

        computerScoreboard.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ReversiTextView scoreboard = new ReversiTextView(GameActivity.this);
                scoreboard.setPadding(convertDpToPx(20), convertDpToPx(2), 0, 0);
                scoreboard.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                scoreboard.setTextSize(25);
                scoreboard.setText("0");
                return scoreboard;
            }
        });

        Animation textIn = AnimationUtils.loadAnimation(this, R.anim.text_fade_in);
        Animation textOut = AnimationUtils.loadAnimation(this, R.anim.text_fade_out);

        userScoreboard.setInAnimation(textIn);
        userScoreboard.setOutAnimation(textOut);

        computerScoreboard.setInAnimation(textIn);
        computerScoreboard.setOutAnimation(textOut);

        userAvatar.setImageDrawable(ContextCompat.getDrawable(this, userAvatarId));
        computerAvatar.setImageDrawable(ContextCompat.getDrawable(this, computerAvatarId));
    }

    private void setupSettingsPanel() {
        settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);

                if (settingsPanel.getVisibility() == View.VISIBLE) {
                    settingsPanel.startAnimation(AnimationUtils.loadAnimation(GameActivity.this,
                            R.anim.scale_dec_panel));
                    settingsPanel.setVisibility(View.GONE);
                } else {
                    settingsPanel.startAnimation(AnimationUtils.loadAnimation(GameActivity.this,
                            R.anim.scale_inc_panel));
                    settingsPanel.setVisibility(View.VISIBLE);
                }
            }
        });

        settingsSoundBtn.setTag("OPEN");
        settingsMusicBtn.setTag("OPEN");

        settingsSoundBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);

                if (settingsSoundBtn.getTag().equals("OPEN")) {
                    settingsSoundBtn.setBackground(
                            ContextCompat.getDrawable(GameActivity.this, R.drawable.settings_sound_icon_muted));
                    settingsSoundBtn.setTag("MUTED");
                    soundMuted = true;

                } else {
                    settingsSoundBtn.setBackground(
                            ContextCompat.getDrawable(GameActivity.this, R.drawable.settings_sound_icon));
                    settingsSoundBtn.setTag("OPEN");
                    soundMuted = false;
                }
            }
        });

        settingsMusicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);

                if (settingsMusicBtn.getTag().equals("OPEN")) {
                    settingsMusicBtn.setBackground(
                            ContextCompat.getDrawable(GameActivity.this, R.drawable.settings_music_icon_muted));
                    settingsMusicBtn.setTag("MUTED");
                    bgMusicServiceIntent.setAction(ReversiConstants.ACTION_SET_MUSIC_VOLUME);
                    bgMusicServiceIntent.putExtra(ReversiConstants.NEW_MUSIC_VOLUME, 0);
                    startService(bgMusicServiceIntent);
                } else {
                    settingsMusicBtn.setBackground(
                            ContextCompat.getDrawable(GameActivity.this, R.drawable.settings_music_icon));
                    settingsMusicBtn.setTag("OPEN");
                    bgMusicServiceIntent.setAction(ReversiConstants.ACTION_SET_MUSIC_VOLUME);
                    bgMusicServiceIntent.putExtra(ReversiConstants.NEW_MUSIC_VOLUME,
                            reversiApp.getSoundOptions().getMusicVolume());
                    startService(bgMusicServiceIntent);
                }
            }
        });


        settingsAboutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aboutDialog.show();
            }
        });
    }

    private void setupResultDialog() {
        resultDialog = new Dialog(this);
        resultDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        resultDialog.setContentView(R.layout.result_dialog);
        resultDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        resultDialog.setCanceledOnTouchOutside(false);
        resultDialog.setCancelable(false);

        ImageButton restartBtn = (ImageButton) resultDialog.findViewById(R.id.result_dialog_restart_btn);
        ImageButton mainMenuBtn = (ImageButton) resultDialog.findViewById(R.id.result_dialog_main_menu_btn);

        restartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);

                restartGame();
                resultDialog.findViewById(R.id.result_loser_tear).setVisibility(View.INVISIBLE);
                resultDialog.findViewById(R.id.result_winner_crown).setVisibility(View.INVISIBLE);
                resultDialog.dismiss();
            }
        });

        mainMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversiApp.playSound(soundMuted, ReversiConstants.CLICK_SOUND);
                reversiApp.setScreen(MainActivity.class.getSimpleName());
                resultDialog.dismiss();
                Intent goToMainMenu = new Intent(GameActivity.this, MainActivity.class);
                startActivity(goToMainMenu);
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
            }
        });
    }

    public void updateScoreboards() {
        userScoreboard.setText(Integer.toString(game.countUserCells()));
        computerScoreboard.setText(Integer.toString(game.countComputerCells()));
    }

    public void showResult(final Integer result) {
        RelativeLayout resultPanelNormal = (RelativeLayout) resultDialog.findViewById(R.id.result_normal);
        ImageView resultWinCrown = (ImageView) resultDialog.findViewById(R.id.result_winner_crown);
        ImageView resultLoseTear = (ImageView) resultDialog.findViewById(R.id.result_loser_tear);
        ImageView avatar = (ImageView) resultDialog.findViewById(R.id.result_avatar_icon);
        ImageView resultUserAvatar = (ImageView) resultDialog.findViewById(R.id.result_dialog_user_avatar);
        ImageView resultComputerAvatar = (ImageView) resultDialog.findViewById(R.id.result_dialog_computer_avatar);
        TextView resultScore = (TextView) resultDialog.findViewById(R.id.result_score);
        final TextView resultText = (TextView) resultDialog.findViewById(R.id.result_text);
        final Animation fadeIn = AnimationUtils.loadAnimation(GameActivity.this, R.anim.text_fade_in);

        resultScore.setText(String.format("%d - %d", game.countUserCells(), game.countComputerCells()));

        avatar.setImageDrawable(ContextCompat.getDrawable(this, userAvatarId));
        resultUserAvatar.setImageDrawable(ContextCompat.getDrawable(this, userAvatarId));
        resultComputerAvatar.setImageDrawable(ContextCompat.getDrawable(this, computerAvatarId));

        if (result == ReversiConstants.WIN) {
            resultPanelNormal.setVisibility(View.VISIBLE);
            resultWinCrown.setVisibility(View.VISIBLE);
            resultText.setVisibility(View.INVISIBLE);
            resultText.setText("You Win!");

            // Pause music
            bgMusicServiceIntent.setAction(ReversiConstants.ACTION_PAUSE_MUSIC);
            startService(bgMusicServiceIntent);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    reversiApp.playSound(soundMuted, ReversiConstants.VICTORY_SOUND);
                    resultText.startAnimation(fadeIn);
                    resultText.setVisibility(View.VISIBLE);

                    // Start again music
                    bgMusicServiceIntent.setAction(ReversiConstants.ACTION_PLAY_MUSIC);
                    startService(bgMusicServiceIntent);
                }
            }, 750);
        } else if (result == ReversiConstants.LOST) {
            resultPanelNormal.setVisibility(View.VISIBLE);
            resultLoseTear.setVisibility(View.VISIBLE);
            resultText.setVisibility(View.INVISIBLE);
            resultText.setText("You Lose!");

            // Pause music
            bgMusicServiceIntent.setAction(ReversiConstants.ACTION_PAUSE_MUSIC);
            startService(bgMusicServiceIntent);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    reversiApp.playSound(soundMuted, ReversiConstants.LOST_SOUND);
                    resultText.startAnimation(fadeIn);
                    resultText.setVisibility(View.VISIBLE);

                    // Start again music
                    bgMusicServiceIntent.setAction(ReversiConstants.ACTION_PLAY_MUSIC);
                    startService(bgMusicServiceIntent);
                }
            }, 750);
        } else if (result == ReversiConstants.DRAW) {
            resultDialog.findViewById(R.id.result_draw).setVisibility(View.VISIBLE);
            resultDialog.show();

            final RelativeLayout clinkHandLeft = ((RelativeLayout) resultDialog.findViewById(R.id.clink_hand_left));
            final RelativeLayout clinkHandRight = ((RelativeLayout) resultDialog.findViewById(R.id.clink_hand_right));
            final ImageView bubbles = (ImageView) resultDialog.findViewById(R.id.bubbles);

            resultText.setVisibility(View.INVISIBLE);
            resultText.setText("Draw!");

            // Pause music
            bgMusicServiceIntent.setAction(ReversiConstants.ACTION_PAUSE_MUSIC);
            startService(bgMusicServiceIntent);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final Animation leftSlideIn = AnimationUtils.loadAnimation(GameActivity.this,
                                    R.anim.left_slide_in);
                            final Animation rightSlideIn = AnimationUtils.loadAnimation(GameActivity.this,
                                    R.anim.right_slide_in);
                            leftSlideIn.setDuration(500);
                            rightSlideIn.setDuration(500);

                            clinkHandLeft.startAnimation(leftSlideIn);
                            clinkHandLeft.setVisibility(View.VISIBLE);

                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    clinkHandRight.startAnimation(rightSlideIn);
                                    clinkHandRight.setVisibility(View.VISIBLE);

                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            bubbles.startAnimation(AnimationUtils.loadAnimation(GameActivity.this,
                                                    R.anim.text_fade_in));
                                            bubbles.setVisibility(View.VISIBLE);

                                            // Start music
                                            bgMusicServiceIntent.setAction(ReversiConstants.ACTION_PLAY_MUSIC);
                                            startService(bgMusicServiceIntent);
                                        }
                                    }, 75);
                                }
                            }, 20);
                        }
                    }, 300);


                    reversiApp.playSound(soundMuted, ReversiConstants.DRAW_SOUND);
                    resultText.startAnimation(fadeIn);
                    resultText.setVisibility(View.VISIBLE);
                }
            }, 750);
        }

        resultDialog.show();

        // Update statistics data
        Intent reversiDataUpdaterIntent = new Intent(this, ReversiDataUpdaterService.class);
        reversiDataUpdaterIntent.setAction(ReversiConstants.ACTION_UPDATE_STAT_DATA);
        reversiDataUpdaterIntent.putExtra(ReversiConstants.RESULT, result);
        reversiDataUpdaterIntent.putExtra(ReversiConstants.USER_CELL_NUM, game.countUserCells());
        reversiDataUpdaterIntent.putExtra(ReversiConstants.SELECTED_USER_AVATAR_NAME, selectedUserAvatarName);
        reversiDataUpdaterIntent.putExtra(ReversiConstants.SELECTED_CPU_AVATAR_NAME, selectedComputerAvatarName);
        startService(reversiDataUpdaterIntent);
    }

    public void setYourTurn(boolean isYourTurn) {
        yourTurn = isYourTurn;
    }

    public Drawable getUserAvatar() {
        return ContextCompat.getDrawable(this, userAvatarId);
    }

    public Drawable getComputerAvatar() {
        return ContextCompat.getDrawable(this, computerAvatarId);
    }

    public Drawable getEmptyPiece() {
        return ContextCompat.getDrawable(this, R.drawable.transparent_bg);
    }

    private int convertDpToPx(int dpVal) {
        int pxVal;

        pxVal = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpVal,
                getResources().getDisplayMetrics());
        return pxVal;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                                View.SYSTEM_UI_FLAG_FULLSCREEN |
                                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
    }

    public Vibrator getWrongMoveVibrator() {
        return wrongMoveVibrator;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent playBgMusic = new Intent(this, BackgroundMusicService.class);
        playBgMusic.setAction(ReversiConstants.ACTION_PLAY_MUSIC);
        startService(playBgMusic);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (reversiApp.getScreen().equals(GameActivity.class.getSimpleName())) {
            Intent pauseBgMusic = new Intent(this, BackgroundMusicService.class);
            pauseBgMusic.setAction(ReversiConstants.ACTION_PAUSE_MUSIC);
            startService(pauseBgMusic);
        }
    }
}
