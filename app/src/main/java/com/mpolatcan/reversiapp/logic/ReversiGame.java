package com.mpolatcan.reversiapp.logic;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.andexert.library.RippleView;
import com.mpolatcan.reversiapp.ReversiBaseApplication;
import com.mpolatcan.reversiapp.utils.ReversiConstants;
import com.mpolatcan.reversiapp.activities.GameActivity;
import com.mpolatcan.reversiapp.activities.MainActivity;

import java.util.ArrayList;

public class ReversiGame
{
    private int turn; // current turn
    private int turnsOpponent; // current turn's opponent
    private ArrayList<ArrayList<Piece>> reversiBoard;
    private Drawable userAvatar;
    private Drawable computerAvatar;
    private Drawable emptyPiece;
    private TableLayout reversiBoardGUI;
    private GameActivity gameActivity;
    private Handler handler;
    private Integer[] gameStatus = new Integer[ReversiConstants.PLAYER_NUM];
    private ReversiBaseApplication reversiApp;

    public ReversiGame(TableLayout reversiBoardGUI, GameActivity gameActivity)
    {
        this.reversiBoardGUI = reversiBoardGUI;
        this.gameActivity = gameActivity;
        reversiBoard = new ArrayList<>();
        reversiApp = ReversiBaseApplication.getInstance();

        userAvatar = gameActivity.getUserAvatar();
        computerAvatar = gameActivity.getComputerAvatar();
        emptyPiece = gameActivity.getEmptyPiece();

        gameStatus[ReversiConstants.USER_GAME_STATUS] = ReversiConstants.CONTINUE;
        gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] = ReversiConstants.CONTINUE;

        handler = new Handler();

        setTurn(ReversiConstants.USER);
        setTurnsOpponent(ReversiConstants.COMPUTER);
        setupBoard();
    }


    private void setupBoard() {
        // Allocate board pieces
        for (int i = 0; i < ReversiConstants.BOARD_SIZE; ++i) {
            reversiBoard.add(i, new ArrayList<Piece>()); // add row to game's data
            for (int j = 0; j < ReversiConstants.BOARD_SIZE; ++j)
                reversiBoard.get(i).add(j, new Piece(ReversiConstants.NONE)); // add column to game's data
        }

        reversiBoard.get((ReversiConstants.BOARD_SIZE / 2) - 1)
                    .set((ReversiConstants.BOARD_SIZE / 2) - 1, new Piece(ReversiConstants.USER));
        reversiBoard.get(ReversiConstants.BOARD_SIZE / 2)
                    .set(ReversiConstants.BOARD_SIZE / 2, new Piece(ReversiConstants.USER));
        reversiBoard.get((ReversiConstants.BOARD_SIZE / 2) - 1)
                    .set((ReversiConstants.BOARD_SIZE / 2), new Piece(ReversiConstants.COMPUTER));
        reversiBoard.get(ReversiConstants.BOARD_SIZE / 2)
                     .set((ReversiConstants.BOARD_SIZE / 2) - 1, new Piece(ReversiConstants.COMPUTER));
    }

    public void setPieceOfBoard(int xCoor, int yCoor, int turn) {
        reversiBoard.get(yCoor).set(xCoor, new Piece(turn));
    }

    public Piece getPieceOfBoard(int xCoor, int yCoor) {
        return reversiBoard.get(yCoor).get(xCoor);
    }

    public void setDrawable(int xCoor, int yCoor, int turn, int duration) {
        ImageView piece = ((ImageView) ((RippleView) ((TableRow) reversiBoardGUI.getChildAt(yCoor))
                                                                                .getChildAt(xCoor))
                                                                                .getChildAt(0));
        Drawable[] pieces = new Drawable[2];

        TransitionDrawable crossfader;

        pieces[0] = piece.getDrawable();

        if (turn == ReversiConstants.USER) {
            pieces[1] = userAvatar;
            piece.setAlpha((float) 1);
            piece.setTag(new Piece(xCoor, yCoor, turn));
        } else if (turn == ReversiConstants.COMPUTER) {
            pieces[1] = computerAvatar;
            piece.setAlpha((float) 1);
            piece.setTag(new Piece(xCoor, yCoor, turn));
        } else if (turn == ReversiConstants.GUIDE) {
            pieces[1] = userAvatar;
            piece.setAlpha((float) 0.30);
            piece.setTag(new Piece(xCoor, yCoor, ReversiConstants.NONE));
        } else {
            pieces[1] = emptyPiece;
            piece.setAlpha((float) 1);
            piece.setTag(new Piece(xCoor, yCoor, ReversiConstants.NONE));
        }

        crossfader = new TransitionDrawable(pieces);
        crossfader.setCrossFadeEnabled(true);
        piece.setImageDrawable(crossfader);

        crossfader.startTransition(duration);

    }

    public void setTurn(int newTurn) {
        turn = newTurn;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurnsOpponent(int newOpponent) {
        turnsOpponent = newOpponent;
    }

    public int getTurnsOpponent() {
        return turnsOpponent;
    }


    public int countUserCells() {
        int userCellNum = 0;

        for (int i = 0; i < ReversiConstants.BOARD_SIZE; ++i)
            for (int j = 0; j < ReversiConstants.BOARD_SIZE; ++j)
                if (reversiBoard.get(i).get(j).getOwner() == ReversiConstants.USER)
                    ++userCellNum;

        return userCellNum;
    }

    public int countComputerCells() {
        int userComputerNum = 0;

        for (int i = 0; i < ReversiConstants.BOARD_SIZE; ++i)
            for (int j = 0; j < ReversiConstants.BOARD_SIZE; ++j)
                if (reversiBoard.get(i).get(j).getOwner() == ReversiConstants.COMPUTER)
                    ++userComputerNum;

        return userComputerNum;
    }

    public void checkGameCompleted() {
        if (isBoardFilled() ||
            (gameStatus[ReversiConstants.USER_GAME_STATUS] == ReversiConstants.CANT_PLAY &&
             gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] == ReversiConstants.CANT_PLAY)) {
            if (countComputerCells() > countUserCells()) {
                gameStatus[ReversiConstants.USER_GAME_STATUS] = ReversiConstants.LOST;
                gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] = ReversiConstants.WIN;
            } else if (countComputerCells() < countUserCells()) {
                gameStatus[ReversiConstants.USER_GAME_STATUS] = ReversiConstants.WIN;
                gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] = ReversiConstants.LOST;
            } else if (countComputerCells() == countUserCells()) {
                gameStatus[ReversiConstants.USER_GAME_STATUS] = ReversiConstants.DRAW;
                gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] = ReversiConstants.DRAW;
            }
        } else if (countUserCells() == 0) {
            gameStatus[ReversiConstants.USER_GAME_STATUS] = ReversiConstants.LOST;
            gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] = ReversiConstants.WIN;
        } else if (countComputerCells() == 0) {
            gameStatus[ReversiConstants.USER_GAME_STATUS] = ReversiConstants.WIN;
            gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] = ReversiConstants.LOST;
        }

        if (gameStatus[ReversiConstants.USER_GAME_STATUS] == ReversiConstants.WIN)
            Log.d("USER-GAME-STATUS", "WIN");
        else if (gameStatus[ReversiConstants.USER_GAME_STATUS] == ReversiConstants.LOST)
            Log.d("USER-GAME-STATUS", "LOST");
        else if (gameStatus[ReversiConstants.USER_GAME_STATUS] == ReversiConstants.CANT_PLAY)
            Log.d("USER-GAME-STATUS", "CANT PLAY");
        else if (gameStatus[ReversiConstants.USER_GAME_STATUS] == ReversiConstants.CONTINUE)
            Log.d("USER-GAME-STATUS", "CONTINUE");

        if (gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] == ReversiConstants.WIN)
            Log.d("COMPUTER-GAME-STATUS", "WIN");
        else if (gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] == ReversiConstants.LOST)
            Log.d("COMPUTER-GAME-STATUS", "LOST");
        else if (gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] == ReversiConstants.CANT_PLAY)
            Log.d("COMPUTER-GAME-STATUS", "CANT PLAY");
        else if (gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] == ReversiConstants.CONTINUE)
            Log.d("COMPUTER-GAME-STATUS", "CONTINUE");
    }

    private boolean isBoardFilled() {
        boolean boardFilled = true;

        for (int i = 0; i < ReversiConstants.BOARD_SIZE; ++i)
            for (int j = 0; j < ReversiConstants.BOARD_SIZE; ++j)
                if (reversiBoard.get(i).get(j).getOwner() == ReversiConstants.NONE)
                    boardFilled = false;

        return boardFilled;
    }

    public void play(Piece clickedPiece) {
        final Integer[] pieceCoors = new Integer[ReversiConstants.COORS];
        final Rules rules = new Rules(this);

        pieceCoors[0] = clickedPiece.getXCoor();
        pieceCoors[1] = clickedPiece.getYCoor();

        final Runnable r2 = new Runnable() {
            @Override
            public void run() {
                Log.d("COMPUTER-TURN", "--- ************* ---");

                checkGameCompleted();

                if (gameStatus[ReversiConstants.USER_GAME_STATUS] == ReversiConstants.CONTINUE ||
                    gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] == ReversiConstants.CONTINUE) {
                    setTurn(ReversiConstants.COMPUTER);
                    setTurnsOpponent(ReversiConstants.USER);

                    rules.setControl(true);

                    if (rules.turnCanPlay()) {
                        gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] = ReversiConstants.CONTINUE;
                        computerAI(rules);
                    } else
                        gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] = ReversiConstants.CANT_PLAY;

                    gameActivity.updateScoreboards();

                    Log.d("BLANK-LINE", " ");
                    Log.d("AFTER-COMPUTER-MOVE", "-------------");

                    setTurn(ReversiConstants.USER);
                    setTurnsOpponent(ReversiConstants.COMPUTER);

                    checkGameCompleted();

                    Log.d("COMPUTER-TURN", "--- ************* ---");


                    if (gameStatus[ReversiConstants.USER_GAME_STATUS] != ReversiConstants.WIN &&
                        gameStatus[ReversiConstants.USER_GAME_STATUS] != ReversiConstants.LOST &&
                        gameStatus[ReversiConstants.USER_GAME_STATUS] != ReversiConstants.DRAW) {
                        rules.setControl(true);
                        rules.setGuideEnabled(false);

                        if (!rules.turnCanPlay())
                            gameStatus[ReversiConstants.USER_GAME_STATUS] = ReversiConstants.CANT_PLAY;
                    } else
                        gameActivity.showResult(gameStatus[ReversiConstants.USER_GAME_STATUS]);


                    if (gameStatus[ReversiConstants.USER_GAME_STATUS] != ReversiConstants.CANT_PLAY)
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                moveGuide();
                                gameActivity.setYourTurn(true);
                            }
                        }, 700);
                    else {
                        handler.postDelayed(this, 2000);
                    }
                } else
                   gameActivity.showResult(gameStatus[ReversiConstants.USER_GAME_STATUS]);
            }
        };

        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                Log.d("USER-TURN", "--- ***************** ---");

                checkGameCompleted();

                if (gameStatus[ReversiConstants.USER_GAME_STATUS] == ReversiConstants.CONTINUE ||
                    gameStatus[ReversiConstants.COMPUTER_GAME_STATUS] == ReversiConstants.CONTINUE) {
                    setTurn(ReversiConstants.USER);
                    setTurnsOpponent(ReversiConstants.COMPUTER);

                    rules.setControl(true);

                    if (rules.turnCanPlay()) {
                        gameStatus[ReversiConstants.USER_GAME_STATUS] = ReversiConstants.CONTINUE;

                        rules.setAiMoveActive(false);
                        rules.setControl(false);

                        if (!rules.validatePosUser(pieceCoors)) {
                            gameActivity.getWrongMoveVibrator().vibrate(50);
                            return;
                        }

                        reversiApp.playSound(gameActivity.soundMuted, ReversiConstants.PUT_SOUND);

                        gameActivity.setYourTurn(false);

                        setTurn(ReversiConstants.COMPUTER);
                        setTurnsOpponent(ReversiConstants.USER);

                        clearCircleGuides();
                    } else {
                        gameActivity.setYourTurn(false);
                        gameStatus[ReversiConstants.USER_GAME_STATUS] = ReversiConstants.CANT_PLAY;
                    }

                    gameActivity.updateScoreboards();

                    Log.d("BLANK-LINE", " ");
                    Log.d("AFTER-USER-MOVE", "-------------");

                    checkGameCompleted();

                    Log.d("USER-TURN", "--- ***************** ---");
                    Log.d("BLANK-LINE", " ");

                    if (gameStatus[ReversiConstants.USER_GAME_STATUS] == ReversiConstants.WIN ||
                        gameStatus[ReversiConstants.USER_GAME_STATUS] == ReversiConstants.LOST ||
                        gameStatus[ReversiConstants.USER_GAME_STATUS] == ReversiConstants.DRAW)
                        gameActivity.showResult(gameStatus[ReversiConstants.USER_GAME_STATUS]);
                    else
                        handler.postDelayed(r2, 1250);
                } else
                    gameActivity.showResult(gameStatus[ReversiConstants.USER_GAME_STATUS]);
            }
        };

        handler.post(r1);
    }

    private void computerAI(final Rules rules) {
        int bestMove = 0;
        final Integer[] bestPosition = new Integer[ReversiConstants.COORS];

        rules.setAiMoveActive(false);
        rules.setControl(false);

        for (int i = 0; i < ReversiConstants.BOARD_SIZE; ++i)
            for (int j = 0; j < ReversiConstants.BOARD_SIZE; ++j)
                if (getPieceOfBoard(j,i).getOwner() == ReversiConstants.NONE)
                    if (bestMove < rules.userPieceCounter(j,i)) {
                        bestMove = rules.userPieceCounter(j,i);
                        bestPosition[0] = j;
                        bestPosition[1] = i;
                    }

        rules.setAiMoveActive(true);
        rules.setControl(false);

        reversiApp.playSound(gameActivity.soundMuted, ReversiConstants.PUT_SOUND);

        rules.makeBestMove(bestPosition);

        rules.setAiMoveActive(false);
    }

    public void moveGuide() {
        Rules rules = new Rules(this);

        setTurn(ReversiConstants.USER);
        setTurnsOpponent(ReversiConstants.COMPUTER);

        rules.setControl(true);
        rules.setGuideEnabled(true);
        rules.turnCanPlay();
    }

    private void clearCircleGuides() {
        for (int i = 0; i < ReversiConstants.BOARD_SIZE; ++i)
            for (int j = 0; j < ReversiConstants.BOARD_SIZE; ++j)
                if (getPieceOfBoard(j, i).getOwner() == ReversiConstants.NONE)
                    setDrawable(j, i, ReversiConstants.NONE, 200);
    }
}