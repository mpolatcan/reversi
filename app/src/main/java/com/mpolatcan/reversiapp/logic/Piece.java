package com.mpolatcan.reversiapp.logic;

/**
 * Created by mpolatcan-gyte_cse on 25.01.2016.
 */
public class Piece {
    private int owner;
    private int xCoor;
    private int yCoor;

    public Piece(int owner) {
        this.owner = owner;
    }

    public Piece(int xCoor, int yCoor, int owner) {
        this.xCoor = xCoor;
        this.yCoor = yCoor;
        this.owner = owner;
    }

    public int getXCoor() {
        return xCoor;
    }

    public int getYCoor() {
        return yCoor;
    }

    public int getOwner() {
        return owner;
    }
}
