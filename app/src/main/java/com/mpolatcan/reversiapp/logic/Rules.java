package com.mpolatcan.reversiapp.logic;

import com.mpolatcan.reversiapp.utils.ReversiConstants;

/**
 * Created by mpolatcan-gyte_cse on 11.11.2015.
 */
public class Rules {
    private ReversiGame game;
    private Integer[] pieceCoors;
    private boolean control;
    private boolean aiMoveActive;
    private boolean guideEnabled;

    public Rules(ReversiGame game) {
        this.game = game;
    }

    public boolean validatePosUser(Integer[] pieceCoors) {
        this.pieceCoors = pieceCoors;

        if (isThatLegal())
            return true;

        return false;
    }

    public int userPieceCounter(int xCoor, int yCoor) {
        final int[] opponentCellNum = {0};

        pieceCoors[0] = xCoor;
        pieceCoors[1] = yCoor;

        opponentCellNum[0] += checkPosLeft();
        opponentCellNum[0] += checkPosRight();
        opponentCellNum[0] += checkPosUp();
        opponentCellNum[0] += checkPosDown();
        opponentCellNum[0] += checkPosDown();
        opponentCellNum[0] += checkPosLeftCrossUp();
        opponentCellNum[0] += checkPosLeftCrossDown();
        opponentCellNum[0] += checkPosRightCrossUp();
        opponentCellNum[0] += checkPosRightCrossDown();

        return opponentCellNum[0];
    }

    public void makeBestMove(Integer[] bestPosition) {
        pieceCoors = bestPosition;

        checkPosLeft();
        checkPosRight();
        checkPosUp();
        checkPosDown();
        checkPosLeftCrossUp();
        checkPosLeftCrossDown();
        checkPosRightCrossUp();
        checkPosRightCrossDown();
    }

    public boolean turnCanPlay() {
        boolean canPlay = false;
        Integer[] position = new Integer[ReversiConstants.COORS];

        for (int i = 0; i < ReversiConstants.BOARD_SIZE; ++i) {
            for (int j = 0; j < ReversiConstants.BOARD_SIZE; ++j) {
                if (game.getPieceOfBoard(j,i).getOwner() == ReversiConstants.NONE) {
                    position[0] = j;
                    position[1] = i;
                    if (validatePosUser(position)) {
                        canPlay = true;
                        if (game.getTurn() == ReversiConstants.USER && guideEnabled)
                            game.setDrawable(position[0],position[1],ReversiConstants.GUIDE,200);
                    }
                }
            }
        }

        return canPlay;
    }

    public void setAiMoveActive(boolean aiMoveActive) {
        this.aiMoveActive = aiMoveActive;
    }

    public void setControl(boolean control) {
        this.control = control;
    }

    public void setGuideEnabled(boolean guideEnabled) { this.guideEnabled = guideEnabled; }

    private boolean isThatLegal() {
        boolean legal = false;

        if (checkPosLeft() > 0)
            legal = true;
        if (checkPosRight() > 0)
            legal = true;
        if (checkPosUp() > 0)
            legal = true;
        if (checkPosDown() > 0)
            legal = true;
        if (checkPosLeftCrossUp() > 0)
            legal = true;
        if (checkPosRightCrossUp() > 0)
            legal = true;
        if (checkPosLeftCrossDown() > 0)
            legal = true;
        if (checkPosRightCrossDown() > 0)
            legal = true;

        return legal;
    }

    private int checkPosLeft() {
        int  foundedCellXCoorLeft = 0,
                requiredOpponentCellNumLeft = 0,
                opponentCellNumLeft = 0;
        boolean foundedLeft = false,
                legalLeft = false;

        /*<---------------------- CHECK LEFT OF xCoor ----------------------->*/
        /* traverse left of xCoor and try to find cell same as current turn */
        for (int i = pieceCoors[0] - 1; i >= 0 && !foundedLeft; --i)
            if (game.getPieceOfBoard(i,pieceCoors[1]).getOwner() ==
                    game.getTurn()) {
                foundedCellXCoorLeft = i;
                foundedLeft = true;
            }

        /* If founded any cells same as current turn */
        if (foundedLeft && (Math.abs(pieceCoors[0] - foundedCellXCoorLeft) > 1)) {
            /*
               Calculate required opponent cell number between founded cell and
               xCoor position
             */
            requiredOpponentCellNumLeft =
                    Math.abs(pieceCoors[0] - foundedCellXCoorLeft - 1);


            if (requiredOpponentCellNumLeft > 0) {
                /*
                    Count cells between founded cell and cell which is at xCoor
                    position
                */
                for (int i = foundedCellXCoorLeft + 1; i < pieceCoors[0]; ++i)
                    if (game.getPieceOfBoard(i,pieceCoors[1]).getOwner()
                            == game.getTurnsOpponent())
                        opponentCellNumLeft++;

                /*
                    If counted cell number is equal to required cell number you
                    can put here
                 */
                if (opponentCellNumLeft == requiredOpponentCellNumLeft)
                    legalLeft = true;
            }
        }
        /*<------------------------------------------------------------------>*/
        if (legalLeft && game.getTurn() == ReversiConstants.USER && !control) {
            makeMoveLeft(foundedCellXCoorLeft);
            return opponentCellNumLeft;
        } else if (legalLeft && game.getTurn() == ReversiConstants.USER && control)
            return opponentCellNumLeft;
        else if (legalLeft && game.getTurn() == ReversiConstants.COMPUTER && !aiMoveActive)
            return opponentCellNumLeft;
        else if (legalLeft && game.getTurn() == ReversiConstants.COMPUTER && aiMoveActive) {
            makeMoveLeft(foundedCellXCoorLeft);
            return opponentCellNumLeft;
        } else
            return 0;
    }

    private int checkPosRight() {
        int foundedCellXCoorRight = 0,
            requiredOpponentCellNumRight = 0,
            opponentCellNumRight = 0;
        boolean foundedRight = false,
                legalRight = false;

        for (int i = pieceCoors[0] + 1; i < ReversiConstants.BOARD_SIZE && !foundedRight; ++i)
            if (game.getPieceOfBoard(i,pieceCoors[1]).getOwner() == game.getTurn()) {
                foundedCellXCoorRight = i;
                foundedRight = true;
            }

        if (foundedRight && (Math.abs(foundedCellXCoorRight - pieceCoors[0]) > 1)) {
            requiredOpponentCellNumRight =
                    Math.abs(foundedCellXCoorRight - pieceCoors[0] - 1);

            if (requiredOpponentCellNumRight > 0) {
                for (int i = pieceCoors[0] + 1; i < foundedCellXCoorRight; ++i)
                    if (game.getPieceOfBoard(i,pieceCoors[1]).getOwner()
                            == game.getTurnsOpponent())
                        opponentCellNumRight++;

                if (opponentCellNumRight == requiredOpponentCellNumRight)
                    legalRight = true;
            }
        }

        if (legalRight && game.getTurn() == ReversiConstants.USER && !control) {
            makeMoveRight(foundedCellXCoorRight);
            return opponentCellNumRight;
        } else if (legalRight && game.getTurn() == ReversiConstants.USER && control)
            return opponentCellNumRight;
        else if (legalRight && game.getTurn() == ReversiConstants.COMPUTER && !aiMoveActive)
            return opponentCellNumRight;
        else if (legalRight && game.getTurn() == ReversiConstants.COMPUTER && aiMoveActive) {
            makeMoveRight(foundedCellXCoorRight);
            return opponentCellNumRight;
        } else
            return 0;
    }

    private int checkPosUp() {
        int foundedCellYCoorUp = 0,
                requiredOpponentCellNumUp = 0,
                opponentCellNumUp = 0;
        boolean foundedUp = false,
                legalUp = false;

        for (int i = pieceCoors[1] - 1; i >= 0 && !foundedUp; --i)
            if (game.getPieceOfBoard(pieceCoors[0],i).getOwner() == game.getTurn()) {
                foundedCellYCoorUp = i;
                foundedUp = true;
            }

        if (foundedUp && Math.abs(pieceCoors[1] - foundedCellYCoorUp) > 1) {
            requiredOpponentCellNumUp = Math.abs(pieceCoors[1] - foundedCellYCoorUp - 1);

            for (int i = foundedCellYCoorUp + 1; i < pieceCoors[1]; ++i)
                if (game.getPieceOfBoard(pieceCoors[0], i).getOwner()
                        == game.getTurnsOpponent())
                    opponentCellNumUp++;

            if (opponentCellNumUp == requiredOpponentCellNumUp)
                legalUp = true;
        }

        if (legalUp && game.getTurn() == ReversiConstants.USER && !control) {
            makeMoveUp(foundedCellYCoorUp);
            return opponentCellNumUp;
        } else if (legalUp && game.getTurn() == ReversiConstants.USER && control)
            return opponentCellNumUp;
        else if (legalUp && game.getTurn() == ReversiConstants.COMPUTER && !aiMoveActive)
            return opponentCellNumUp;
        else if (legalUp && game.getTurn() == ReversiConstants.COMPUTER && aiMoveActive) {
            makeMoveUp(foundedCellYCoorUp);
            return opponentCellNumUp;
        }
        else
            return 0;
    }

    private int checkPosDown() {
        int foundedCellYCoorDown = 0,
                requiredOpponentCellNumDown = 0,
                opponentCellNumDown = 0;
        boolean foundedDown = false,
                legalDown = false;

        for (int i = pieceCoors[1] + 1; i < ReversiConstants.BOARD_SIZE && !foundedDown; ++i)
            if (game.getPieceOfBoard(pieceCoors[0],i).getOwner() == game.getTurn()) {
                foundedCellYCoorDown = i;
                foundedDown = true;
            }

        if (foundedDown && (Math.abs(foundedCellYCoorDown - pieceCoors[1]) > 1)) {
            requiredOpponentCellNumDown =
                    Math.abs(foundedCellYCoorDown - pieceCoors[1] - 1);

            for (int i = pieceCoors[1] + 1; i < foundedCellYCoorDown; ++i)
                if (game.getPieceOfBoard(pieceCoors[0],i).getOwner()
                        == game.getTurnsOpponent())
                    opponentCellNumDown++;

            if (opponentCellNumDown == requiredOpponentCellNumDown)
                legalDown = true;
        }

        if (legalDown && game.getTurn() == ReversiConstants.USER && !control) {
            makeMoveDown(foundedCellYCoorDown);
            return opponentCellNumDown;
        } else if (legalDown && game.getTurn() == ReversiConstants.USER && control)
            return opponentCellNumDown;
        else if (legalDown && game.getTurn() == ReversiConstants.COMPUTER && !aiMoveActive)
            return opponentCellNumDown;
        else if (legalDown && game.getTurn() == ReversiConstants.COMPUTER && aiMoveActive) {
            makeMoveDown(foundedCellYCoorDown);
            return opponentCellNumDown;
        }
        else
            return 0;
    }

    private int checkPosLeftCrossUp() {
        int[] foundedCellLeftCrossUp = new int[ReversiConstants.COORS];
        int requiredOpponentCellLeftCrossUp = 0,
                opponentCellNumLeftCrossUp = 0;
        boolean foundedLeftCrossUp = false,
                legalLeftCrossUp = false;

        for (int i = pieceCoors[1] - 1, j = pieceCoors[0] - 1;
             i >= 0 && j >= 0 && foundedLeftCrossUp == false;
             --i, --j) {
            if (game.getPieceOfBoard(j,i).getOwner() == game.getTurn()) {
                foundedCellLeftCrossUp[0] = j;
                foundedCellLeftCrossUp[1] = i;
                foundedLeftCrossUp = true;
            }
        }

        if (foundedLeftCrossUp &&
                Math.abs(pieceCoors[0] - foundedCellLeftCrossUp[0]) > 1 &&
                Math.abs(pieceCoors[1] - foundedCellLeftCrossUp[1]) > 1) {

            requiredOpponentCellLeftCrossUp =
                    Math.abs(pieceCoors[0] - foundedCellLeftCrossUp[0] - 1);

            for (int i = foundedCellLeftCrossUp[1] + 1,
                 j = foundedCellLeftCrossUp[0] + 1;
                 i < pieceCoors[1] && j < pieceCoors[0];
                 ++i, ++j) {
                if (game.getPieceOfBoard(j,i).getOwner() == game.getTurnsOpponent())
                    ++opponentCellNumLeftCrossUp;
            }

            if (opponentCellNumLeftCrossUp == requiredOpponentCellLeftCrossUp)
                legalLeftCrossUp = true;
        }

        if (legalLeftCrossUp && game.getTurn() == ReversiConstants.USER && !control) {
            makeMoveLeftCrossUp(foundedCellLeftCrossUp);
            return opponentCellNumLeftCrossUp;
        } else if (legalLeftCrossUp && game.getTurn() == ReversiConstants.USER && control)
            return opponentCellNumLeftCrossUp;
        else if (legalLeftCrossUp && game.getTurn() == ReversiConstants.COMPUTER && !aiMoveActive)
            return opponentCellNumLeftCrossUp;
        else if (legalLeftCrossUp && game.getTurn() == ReversiConstants.COMPUTER && aiMoveActive) {
            makeMoveLeftCrossUp(foundedCellLeftCrossUp);
            return opponentCellNumLeftCrossUp;
        } else
            return 0;
    }

    private int checkPosLeftCrossDown() {
        int[] foundedCellLeftCrossDown = new int[ReversiConstants.COORS];
        int requiredOpponentCellLeftCrossDown = 0,
                opponentCellNumLeftCrossDown = 0;
        boolean foundedLeftCrossDown = false,
                legalLeftCrossDown = false;

        for (int i = pieceCoors[1] + 1, j = pieceCoors[0] + 1;
             i < ReversiConstants.BOARD_SIZE && j < ReversiConstants.BOARD_SIZE &&
                     foundedLeftCrossDown == false;
             ++i, ++j)
            if (game.getPieceOfBoard(j,i).getOwner() == game.getTurn()) {
                foundedCellLeftCrossDown[0] = j;
                foundedCellLeftCrossDown[1] = i;
                foundedLeftCrossDown = true;
            }

        if (foundedLeftCrossDown &&
                Math.abs(foundedCellLeftCrossDown[0] - pieceCoors[0]) > 1 &&
                Math.abs(foundedCellLeftCrossDown[1] - pieceCoors[1]) > 1) {

            requiredOpponentCellLeftCrossDown =
                    Math.abs(foundedCellLeftCrossDown[0] - pieceCoors[0] - 1);

            for (int i = pieceCoors[1] + 1, j = pieceCoors[0] + 1;
                 i < foundedCellLeftCrossDown[1] &&
                         j < foundedCellLeftCrossDown[0];
                 ++i, ++j)
                if (game.getPieceOfBoard(j,i).getOwner() == game.getTurnsOpponent())
                    ++opponentCellNumLeftCrossDown;

            if (opponentCellNumLeftCrossDown == requiredOpponentCellLeftCrossDown)
                legalLeftCrossDown = true;
        }

        if (legalLeftCrossDown && game.getTurn() == ReversiConstants.USER && !control) {
            makeMoveLeftCrossDown(foundedCellLeftCrossDown);
            return opponentCellNumLeftCrossDown;
        } else if (legalLeftCrossDown && game.getTurn() == ReversiConstants.USER && control)
            return opponentCellNumLeftCrossDown;
        else if (legalLeftCrossDown && game.getTurn() == ReversiConstants.COMPUTER && !aiMoveActive)
            return opponentCellNumLeftCrossDown;
        else if (legalLeftCrossDown && game.getTurn() == ReversiConstants.COMPUTER && aiMoveActive) {
            makeMoveLeftCrossDown(foundedCellLeftCrossDown);
            return opponentCellNumLeftCrossDown;
        } else
            return 0;
    }

    private int checkPosRightCrossUp() {
        int[] foundedCellRightCrossUp = new int[ReversiConstants.COORS];
        int requiredOpponentCellRightCrossUp = 0,
                opponentCellNumRightCrossUp = 0;
        boolean foundedRightCrossUp = false,
                legalRightCrossUp = false;

        for (int i = pieceCoors[1] - 1, j = pieceCoors[0] + 1;
             i >= 0 && j < ReversiConstants.BOARD_SIZE && foundedRightCrossUp == false;
             --i, ++j) {
            if (game.getPieceOfBoard(j,i).getOwner() == game.getTurn()) {
                foundedCellRightCrossUp[0] = j;
                foundedCellRightCrossUp[1] = i;
                foundedRightCrossUp = true;
            }
        }

        if (foundedRightCrossUp &
                Math.abs(foundedCellRightCrossUp[0] - pieceCoors[0]) > 1 &&
                Math.abs(foundedCellRightCrossUp[1] - pieceCoors[1]) > 1) {

            requiredOpponentCellRightCrossUp = Math.abs(foundedCellRightCrossUp[0] - pieceCoors[0] - 1);

            for (int i = pieceCoors[1] - 1, j = pieceCoors[0] + 1;
                 i >= foundedCellRightCrossUp[1] && j < foundedCellRightCrossUp[0];
                 --i, ++j) {
                if (game.getPieceOfBoard(j,i).getOwner() == game.getTurnsOpponent())
                    ++opponentCellNumRightCrossUp;
            }

            if (opponentCellNumRightCrossUp == requiredOpponentCellRightCrossUp)
                legalRightCrossUp = true;
        }

        if (legalRightCrossUp && game.getTurn() == ReversiConstants.USER && !control) {
            makeMoveRightCrossUp(foundedCellRightCrossUp);
            return opponentCellNumRightCrossUp;
        } else if (legalRightCrossUp && game.getTurn() == ReversiConstants.USER && control)
            return opponentCellNumRightCrossUp;
        else if (legalRightCrossUp && game.getTurn() == ReversiConstants.COMPUTER && !aiMoveActive)
            return opponentCellNumRightCrossUp;
        else if (legalRightCrossUp && game.getTurn() == ReversiConstants.COMPUTER && aiMoveActive) {
            makeMoveRightCrossUp(foundedCellRightCrossUp);
            return opponentCellNumRightCrossUp;
        } else
            return 0;
    }

    private int checkPosRightCrossDown() {
        int[] foundedCellRightCrossDown = new int[ReversiConstants.COORS];
        int requiredOpponentCellRightCrossDown = 0,
                opponentCellNumRightCrossDown = 0;
        boolean foundedRightCrossDown = false,
                legalRightCrossDown = false;

        for (int i = pieceCoors[1] + 1, j = pieceCoors[0] - 1;
             i < ReversiConstants.BOARD_SIZE && j >= 0 && foundedRightCrossDown == false;
             ++i, --j) {
            if (game.getPieceOfBoard(j,i).getOwner() == game.getTurn()) {
                foundedCellRightCrossDown[0] = j;
                foundedCellRightCrossDown[1] = i;
                foundedRightCrossDown = true;
            }
        }

        if (foundedRightCrossDown && Math.abs(foundedCellRightCrossDown[0] - pieceCoors[0]) > 1 &&
                Math.abs(foundedCellRightCrossDown[1] - pieceCoors[1]) > 1) {

            requiredOpponentCellRightCrossDown =
                    Math.abs(pieceCoors[0] - foundedCellRightCrossDown[0] - 1);

            for (int i = pieceCoors[1] + 1, j = pieceCoors[0] - 1;
                 i < foundedCellRightCrossDown[1] && j > foundedCellRightCrossDown[0];
                 ++i, --j) {
                if (game.getPieceOfBoard(j,i).getOwner() == game.getTurnsOpponent())
                    ++opponentCellNumRightCrossDown;
            }

            if (opponentCellNumRightCrossDown == requiredOpponentCellRightCrossDown)
                legalRightCrossDown = true;
        }

        if (legalRightCrossDown && game.getTurn() == ReversiConstants.USER && !control) {
            makeMoveRightCrossDown(foundedCellRightCrossDown);
            return opponentCellNumRightCrossDown;
        } else if (legalRightCrossDown && game.getTurn() == ReversiConstants.USER && control)
            return opponentCellNumRightCrossDown;
        else if (legalRightCrossDown && game.getTurn() == ReversiConstants.COMPUTER && !aiMoveActive) {
            return opponentCellNumRightCrossDown;
        } else if (legalRightCrossDown && game.getTurn() == ReversiConstants.COMPUTER && aiMoveActive) {
            makeMoveRightCrossDown(foundedCellRightCrossDown);
            return opponentCellNumRightCrossDown;
        } else
            return 0;
    }

    private void makeMoveLeft(int foundedCellXCoorLeft) {
        game.setPieceOfBoard(pieceCoors[0], pieceCoors[1], game.getTurn());
        game.setDrawable(pieceCoors[0], pieceCoors[1], game.getTurn(), 500);


        for (int i = foundedCellXCoorLeft + 1, counter = 1; i < pieceCoors[0]; ++i, ++counter) {
            game.setPieceOfBoard(i, pieceCoors[1], game.getTurn());
            game.setDrawable(i, pieceCoors[1], game.getTurn(), 500 + (counter  * 150));
        }
    }

    private void makeMoveRight(int foundedCellXCoorRight) {
        game.setPieceOfBoard(pieceCoors[0], pieceCoors[1], game.getTurn());
        game.setDrawable(pieceCoors[0], pieceCoors[1], game.getTurn(), 500);


        for (int i = pieceCoors[0] + 1, counter = 1; i < foundedCellXCoorRight; ++i, ++counter) {
            game.setPieceOfBoard(i, pieceCoors[1], game.getTurn());
            game.setDrawable(i, pieceCoors[1], game.getTurn(), 500 + (counter * 150));
        }
    }


    private void makeMoveUp(int foundedCellYCoorUp) {
        game.setPieceOfBoard(pieceCoors[0], pieceCoors[1], game.getTurn());
        game.setDrawable(pieceCoors[0], pieceCoors[1], game.getTurn(), 500);


        for (int i = foundedCellYCoorUp + 1, counter = 1; i < pieceCoors[1]; ++i, ++counter) {
            game.setPieceOfBoard(pieceCoors[0], i, game.getTurn());
            game.setDrawable(pieceCoors[0], i, game.getTurn(), 500 + (counter * 150));

        }
    }

    private void makeMoveDown(final int foundedCellYCoorDown) {
       game.setPieceOfBoard(pieceCoors[0], pieceCoors[1], game.getTurn());
       game.setDrawable(pieceCoors[0], pieceCoors[1], game.getTurn(), 500);

        for (int i = pieceCoors[1] + 1, counter = 1; i < foundedCellYCoorDown; ++i, ++counter) {
            game.setPieceOfBoard(pieceCoors[0], i, game.getTurn());
            game.setDrawable(pieceCoors[0], i, game.getTurn(), 500 + (counter * 150));
        }
    }

    private void makeMoveLeftCrossUp(int[] foundedCellLeftCrossUp) {
        game.setPieceOfBoard(pieceCoors[0], pieceCoors[1], game.getTurn());
        game.setDrawable(pieceCoors[0], pieceCoors[1], game.getTurn(), 500);

        for (int i = foundedCellLeftCrossUp[1] + 1, j = foundedCellLeftCrossUp[0] + 1, counter = 1;
             i < pieceCoors[1] && j < pieceCoors[0]; ++i, ++j, ++counter) {
            game.setPieceOfBoard(j, i, game.getTurn());
            game.setDrawable(j, i, game.getTurn(), 500 + (counter * 150));
        }
    }

    private void makeMoveLeftCrossDown(int[] foundedCellLeftCrossDown){
        game.setPieceOfBoard(pieceCoors[0], pieceCoors[1], game.getTurn());
        game.setDrawable(pieceCoors[0], pieceCoors[1], game.getTurn(), 500);


        for (int i = pieceCoors[1] + 1, j = pieceCoors[0] + 1, counter = 1;
             i < foundedCellLeftCrossDown[1] && j < foundedCellLeftCrossDown[0];
             ++i, ++j, ++counter) {
            game.setPieceOfBoard(j, i, game.getTurn());
            game.setDrawable(j, i, game.getTurn(), 500 + (counter * 150));
        }
    }

    private void makeMoveRightCrossUp(int[] foundedCellRightCrossUp) {
        game.setPieceOfBoard(pieceCoors[0], pieceCoors[1], game.getTurn());
        game.setDrawable(pieceCoors[0], pieceCoors[1], game.getTurn(), 500);

        for (int i = pieceCoors[1] - 1, j = pieceCoors[0] + 1, counter = 1;
             i >= foundedCellRightCrossUp[1] && j < foundedCellRightCrossUp[0];
             --i, ++j, ++counter) {
            game.setPieceOfBoard(j, i, game.getTurn());
            game.setDrawable(j, i, game.getTurn(), 500 + (counter * 150));

        }
    }

    private void makeMoveRightCrossDown(int[] foundedCellRightCrossDown) {
        game.setPieceOfBoard(pieceCoors[0], pieceCoors[1], game.getTurn());
        game.setDrawable(pieceCoors[0], pieceCoors[1], game.getTurn(), 500);


        for (int i = pieceCoors[1] + 1, j = pieceCoors[0] - 1, counter = 1;
             i < foundedCellRightCrossDown[1] && j >= foundedCellRightCrossDown[0];
             ++i, --j, ++counter) {
            game.setPieceOfBoard(j, i, game.getTurn());
            game.setDrawable(j, i, game.getTurn(), 500 + (counter * 150));

        }
    }
}
