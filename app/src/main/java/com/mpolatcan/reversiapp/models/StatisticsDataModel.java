package com.mpolatcan.reversiapp.models;

/**
 * Created by mpolatcan-gyte_cse on 01.08.2016.
 */
public class StatisticsDataModel {
    private int winNumber;
    private int lostNumber;
    private int highScore;
    private int mostBeatenAvatar;
    private int mostPlayedAvatar;

    public StatisticsDataModel() {
        winNumber = 0;
        lostNumber = 0;
        highScore = 0;
        mostBeatenAvatar = 0;
        mostPlayedAvatar = 0;
    }

    public void setWinNumber(int winNumber) {
        this.winNumber = winNumber;
    }

    public void setLostNumber(int lostNumber) {
        this.lostNumber = lostNumber;
    }

    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }

    public void setMostPlayedAvatar(int mostPlayedAvatar) {
        this.mostPlayedAvatar = mostPlayedAvatar;
    }

    public void setMostBeatenAvatar(int mostBeatenAvatar) {
        this.mostBeatenAvatar = mostBeatenAvatar;
    }

    public int getWinNumber() {
        return winNumber;
    }

    public int getLostNumber() {
        return lostNumber;
    }

    public int getHighScore() {
        return highScore;
    }

    public int getMostPlayedAvatar() {
        return mostPlayedAvatar;
    }

    public int getMostBeatenAvatar() {
        return mostBeatenAvatar;
    }
}
