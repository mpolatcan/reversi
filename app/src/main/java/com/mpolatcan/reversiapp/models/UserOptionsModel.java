package com.mpolatcan.reversiapp.models;

import android.util.Log;

import com.mpolatcan.reversiapp.utils.ReversiConstants;

/**
 * Created by mpolatcan-gyte_cse on 01.08.2016.
 */
public class UserOptionsModel {
    private int lastSelectedAvatar;
    private int lastSelectedBoard;
    private int isAvatarUnlocked;
    private int unlockedAvatarIndex;

    public UserOptionsModel() {
        Log.d("CREATE-AGAIN", "HERE");
        lastSelectedAvatar = 0;
        lastSelectedBoard = 0;
        unlockedAvatarIndex = 0;
        isAvatarUnlocked = ReversiConstants.NOT_UNLOCKED;
    }

    public void setLastSelectedAvatar(int lastSelectedAvatar) {
        this.lastSelectedAvatar = lastSelectedAvatar;
    }

    public void setLastSelectedBoard(int lastSelectedBoard) {
        this.lastSelectedBoard = lastSelectedBoard;
    }

    public void setIsAvatarUnlocked(int isAvatarUnlocked) {
        this.isAvatarUnlocked = isAvatarUnlocked;
    }

    public void setUnlockedAvatarIndex(int unlockedAvatarIndex) {
        this.unlockedAvatarIndex = unlockedAvatarIndex;
    }

    public int getLastSelectedAvatar() {
        return lastSelectedAvatar;
    }

    public int getLastSelectedBoard() {
        return lastSelectedBoard;
    }

    public int getIsAvatarUnlocked() {
        return isAvatarUnlocked;
    }

    public int getUnlockedAvatarIndex() {
        return unlockedAvatarIndex;
    }
}
