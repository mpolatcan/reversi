package com.mpolatcan.reversiapp.models;

/**
 * Created by mpolatcan-gyte_cse on 01.08.2016.
 */
public class SoundOptionsModel {
    private int sfxVolume;
    private int musicVolume;

    public SoundOptionsModel() {
        sfxVolume = 100;
        musicVolume = 100;
    }

    public void setSfxVolume(int sfxVolume) {
        this.sfxVolume = sfxVolume;
    }

    public void setMusicVolume(int musicVolume) {
        this.musicVolume = musicVolume;
    }

    public int getSfxVolume() {
        return sfxVolume;
    }

    public int getMusicVolume() {
        return musicVolume;
    }
}
