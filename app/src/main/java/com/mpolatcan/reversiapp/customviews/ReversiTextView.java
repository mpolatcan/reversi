package com.mpolatcan.reversiapp.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mpolatcan.reversiapp.R;

/**
 * Created by mpolatcan-gyte_cse on 22.07.2016.
 */
public class ReversiTextView extends TextView {
    private Typeface typeface;
    private TypedArray attrs;

    public ReversiTextView(Context context) {
        super(context);
        initTypeface(context, "CarterOne.ttf");
        this.setTypeface(typeface);
        this.setShadowLayer(5, 0, 2, ContextCompat.getColor(context, R.color.md_black_1000));
        this.setTextColor(ContextCompat.getColor(context, R.color.md_white_1000));
    }

    public ReversiTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.attrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ReversiTextView, 0, 0);
        initTypeface(context, this.attrs.getString(R.styleable.ReversiTextView_typeface));
        this.setTypeface(typeface);
        this.setShadowLayer(5, 0, 2, ContextCompat.getColor(context, R.color.md_black_1000));
        this.setTextColor(this.attrs.getColor(R.styleable.ReversiTextView_textColor, 0));
    }

    public ReversiTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.attrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ReversiTextView, 0, 0);
        initTypeface(context, this.attrs.getString(R.styleable.ReversiTextView_typeface));
        this.setTypeface(typeface);
        this.setShadowLayer(5, 0, 2, ContextCompat.getColor(context, R.color.md_black_1000));
        this.setTextColor(this.attrs.getColor(R.styleable.ReversiTextView_textColor, 0));
    }

    private void initTypeface(Context context, String string) {
        typeface = Typeface.createFromAsset(context.getAssets(), string);
    }
}
