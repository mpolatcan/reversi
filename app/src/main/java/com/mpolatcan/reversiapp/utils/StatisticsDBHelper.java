package com.mpolatcan.reversiapp.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mpolatcan.reversiapp.R;
import com.mpolatcan.reversiapp.utils.ReversiConstants;

import java.util.ArrayList;

/**
 * Created by mpolatcan-gyte_cse on 01.07.2016.
 */

/* SQLiteOpenHelper wraps up the logic to create and upgrade a database, per your
specifications, as needed by your application */
public class StatisticsDBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "avatarrates.db";
    private static final int SCHEMA_VERSION = 1;
    private ArrayList<String> avatarFilenames = new ArrayList<>();

    public StatisticsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA_VERSION);

        TypedArray filenames = context.getResources().obtainTypedArray(R.array.reversi_avatar_filenames);

        for (int i = 0; i < filenames.length(); ++i)
            avatarFilenames.add(filenames.getString(i));

        filenames.recycle();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE AvatarRates (avatar_name TEXT, play_rate INTEGER, " +
                "beat_rate INTEGER);");

        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < avatarFilenames.size(); ++i) {
            contentValues.put(ReversiConstants.AVATAR_NAME, avatarFilenames.get(i));
            contentValues.put(ReversiConstants.PLAY_RATE, 0);
            contentValues.put(ReversiConstants.BEAT_RATE, 0);
            db.insert(ReversiConstants.TABLE_NAME, ReversiConstants.AVATAR_NAME,
                    contentValues);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        throw new RuntimeException("Database schema not upgraded yet!");
    }
}
