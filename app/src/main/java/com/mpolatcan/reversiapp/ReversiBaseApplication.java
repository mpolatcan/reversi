package com.mpolatcan.reversiapp;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;

import com.mpolatcan.reversiapp.activities.MainActivity;
import com.mpolatcan.reversiapp.models.SoundOptionsModel;
import com.mpolatcan.reversiapp.models.StatisticsDataModel;
import com.mpolatcan.reversiapp.models.UserOptionsModel;
import com.mpolatcan.reversiapp.services.ReversiDataUpdaterService;
import com.mpolatcan.reversiapp.utils.ReversiConstants;

/**
 * Created by mpolatcan-gyte_cse on 11.08.2016.
 */
public class ReversiBaseApplication extends Application {
    private String whichScreen = MainActivity.class.getSimpleName();
    private static ReversiBaseApplication singleton;
    private UserOptionsModel userOptions;
    private StatisticsDataModel statisticsData;
    private SoundOptionsModel soundOptions;
    private int clickSoundId,
               victorySoundId,
               loseSoundId,
               putSoundId,
               selectSoundId,
               unlockedSoundId;
    private SoundPool soundPool;
    private boolean isGameClosed = false;

    public static ReversiBaseApplication getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        userOptions = new UserOptionsModel();
        statisticsData = new StatisticsDataModel();
        soundOptions = new SoundOptionsModel();

        prepareClickSound();

        clickSoundId = soundPool.load(this, R.raw.button_click, 1);
        victorySoundId = soundPool.load(this, R.raw.victory_sound, 1);
        loseSoundId = soundPool.load(this, R.raw.lost_sound, 1);
        putSoundId = soundPool.load(this, R.raw.put_sound, 1);
        selectSoundId = soundPool.load(this, R.raw.select_sound, 1);
        unlockedSoundId = soundPool.load(this, R.raw.unlocked_avatar, 1);

        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                Log.d("APP", "Sounds are loaded");
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                Intent reversiDataUpdaterIntent = new Intent(ReversiBaseApplication.this, ReversiDataUpdaterService.class);
                reversiDataUpdaterIntent.setAction(ReversiConstants.ACTION_FETCH_DATA);
                startService(reversiDataUpdaterIntent);
            }
        }).start();


        singleton = this;
    }

    private void prepareClickSound() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            createNewSoundPool();
        else
            createOldSoundPool();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void createNewSoundPool() {
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        soundPool = new SoundPool.Builder().setAudioAttributes(attributes).build();
    }

    @SuppressWarnings("deprecation")
    private void createOldSoundPool() {
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
    }

    public void playSound(boolean soundMuted, String soundType) {
        int volume;

        if (soundMuted)
            volume = 0;
        else
            volume = soundOptions.getSfxVolume();

        final float newVolume = (float) (1 - (Math.log(ReversiConstants.MAX_VOLUME - volume)
                / Math.log(ReversiConstants.MAX_VOLUME)));

        if (soundType.equals(ReversiConstants.CLICK_SOUND))
            soundPool.play(clickSoundId, newVolume, newVolume, 1, 0, 1.0f);
        else if (soundType.equals(ReversiConstants.VICTORY_SOUND))
            soundPool.play(victorySoundId, newVolume, newVolume, 1, 0, 1.0f);
        else if (soundType.equals(ReversiConstants.LOST_SOUND))
            soundPool.play(loseSoundId, newVolume, newVolume, 1, 0, 1.0f);
        else if (soundType.equals(ReversiConstants.PUT_SOUND))
            soundPool.play(putSoundId, newVolume, newVolume, 1, 0, 1.0f);
        else if (soundType.equals(ReversiConstants.SELECT_SOUND))
            soundPool.play(selectSoundId, newVolume, newVolume, 1, 0, 1.0f);
        else if (soundType.equals(ReversiConstants.UNLOCKED_SOUND))
            soundPool.play(unlockedSoundId, newVolume, newVolume, 1, 0, 1.0f);
    }

    public UserOptionsModel getUserOptions() {
        return userOptions;
    }

    public SoundOptionsModel getSoundOptions() {
        return soundOptions;
    }

    public StatisticsDataModel getStatisticsData() {
        return statisticsData;
    }

    public String getScreen() {
        return whichScreen;
    }

    public void setScreen(String screen) {
        Log.d("ACTUAL-SCREEN", screen);
        whichScreen = screen;
    }

    public boolean isGameClosed() {
        return isGameClosed;
    }

    public void setGameClosed(boolean gameClosed) {
        isGameClosed = gameClosed;
    }
}
