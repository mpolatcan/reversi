-dontusemixedcaseclassnames
-optimizationpasses 2
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-printmapping mapping.txt
-printusage usage.txt
-printseeds seeds.txt
-allowaccessmodification
-dontpreverify
-keep class org.apache.http.** { *; }
-keep class com.google.android.gms.** { *;}
-keep class com.instabug.library.** { *; }
-keep class com.crashlytics.android.** { *; }
-keep class io.fabric.sdk.** { *; }
-dontwarn org.apache.http.**
-dontwarn com.google.android.gms.**
-dontwarn com.instabug.library.**
-dontwarn com.crashlytics.android.**
-dontwarn io.fabric.sdk.**

-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}

-assumenosideeffects class android.os.StrictMode {
    public static void setThreadPolicy(...);
    public static void setVmPolicy(...);
}
